package com.ood.ordering.controller;

import android.app.Application;

import com.ood.ordering.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class Ordering extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

}
