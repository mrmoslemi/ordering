package com.ood.ordering.controller.Requester;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.ood.ordering.models.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Lenovo on 3/29/2017.
 */

public class Handler extends JsonHttpResponseHandler {
    private Request request;
    private Requester requester;

    Handler(Request request, Requester requester) {
        this.request = request;
        this.requester = requester;
    }

    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            if (request.getWaiter() instanceof Requester.PaginationListWaiter) {
                try {
                    Gson gson = new Gson();
                    String next = response.getString("next");
                    String previous = response.getString("previous");
                    ArrayList<Model> arrayResult = new ArrayList<>();
                    JSONArray result = response.getJSONArray("results");
                    int i = 0;
                    while (true) {
                        try {
                            JSONObject object = result.getJSONObject(i);
                            Model resultObject = gson.fromJson(object.toString(), request.getObjectsClass());
                            arrayResult.add(resultObject);
                            i++;
                        } catch (JSONException e) {
                            break;
                        }
                    }
                    boolean hasNext = !(next == null || next.isEmpty() || next.equals("null"));
                    boolean hasPrevious = !(previous == null || previous.isEmpty() || previous.equals("null"));
                    requester.success(request);
                    ((Requester.PaginationListWaiter) request.getWaiter()).onSuccess(statusCode, request.getID(), arrayResult, hasNext, hasPrevious);
                } catch (JSONException e) {
                    requester.success(request);
                    request.getWaiter().onFailure(statusCode, request.getID());

                }
            } else if (request.getWaiter() instanceof Requester.ObjectWaiter) {
                Gson gson = new Gson();
                Model responseObject = gson.fromJson(response.toString(), request.getObjectsClass());
                requester.success(request);
                ((Requester.ObjectWaiter) request.getWaiter()).onSuccess(statusCode, request.getID(), responseObject);
            }
        }
    }

    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            if (request.getWaiter() instanceof Requester.ListWaiter) {
                Gson gson = new Gson();
                ArrayList<Model> toReturn = new ArrayList<>();
                try {
                    int i = 0;
                    while (true) {
                        Model responseObject = gson.fromJson(response.getJSONObject(i).toString(), request.getObjectsClass());
                        toReturn.add(responseObject);
                        i++;
                    }
                } catch (JSONException e) {
                    requester.success(request);
                    ((Requester.ListWaiter) request.getWaiter()).onSuccess(statusCode, request.getID(), toReturn);
                }
            } else if (request.getWaiter() instanceof Requester.ResponseWaiter) {
                requester.success(request);
                ((Requester.ResponseWaiter) request.getWaiter()).onSuccess(statusCode, request.getID());
            }
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            if (request.getWaiter() instanceof Requester.ResponseWaiter) {
                requester.success(request);
                ((Requester.ResponseWaiter) request.getWaiter()).onSuccess(statusCode, request.getID());
            }
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            if (isSuccess(statusCode)) {
                requester.success(request);
                onSuccess(statusCode, headers, errorResponse);
            } else if (checkAuthentication(statusCode)) {
                requester.success(request);
                request.getWaiter().onFailure(statusCode, request.getID());
            }
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            requester.success(request);
            if (isSuccess(statusCode)) {
                onSuccess(statusCode, headers, errorResponse);
            } else {
                request.getWaiter().onFailure(statusCode, request.getID());

            }
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        if (!(request.isAuthenticated() && requester.isHandlingFailure())) {
            requester.success(request);
            if (isSuccess(statusCode)) {
                onSuccess(statusCode, headers, responseString);
            } else {
                request.getWaiter().onFailure(statusCode, request.getID());
            }
        }
    }

    private boolean checkAuthentication(int statusCode) {
        if (!request.isAuthenticated()) {
            return true;
        } else if (statusCode == Request.RESPONSE_NOT_AUTHENTICATED) {
            requester.authenticationFailure();
            return false;
        } else {
            return true;
        }
    }

    private boolean isSuccess(int statusCode) {
        return statusCode / 100 == 2;
    }
}
