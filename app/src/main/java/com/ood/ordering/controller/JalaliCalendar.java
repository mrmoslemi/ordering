package com.ood.ordering.controller;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.ood.ordering.R;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class JalaliCalendar {
    //    public static void main(String[] args) {
//        Date today = new Date();
//        int week = today.getDay();
//        int month = today.getMonth();
//        int day = today.getDate();
//        int year = today.getYear();
//        System.out.print(gregorian_to_jalali(2017 + 1900, month + 1, day, week) + "\n");
//    }
    public static float dpToPx(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static String gregorian_to_jalali(String georgian) {
        String[] splited = georgian.split("-");

        int gy1, gm1, gd1;
        gy1 = Integer.parseInt(splited[0]);
        gm1 = Integer.parseInt(splited[1]);
        gd1 = Integer.parseInt(splited[2]);

        int g_days_in_month[] = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int j_days_in_month[] = new int[]{31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};
        int gy = gy1 - 1600;
        int gm = gm1 - 1;
        int gd = gd1 - 1;
        int g_day_no =
                365 * gy + doubleToInt((gy + 3) / 4) - doubleToInt((gy + 99) / 100) +
                        doubleToInt((gy + 399) / 400);
        int i;
        for (i = 0; i < gm; ++i)
            g_day_no += g_days_in_month[i];
        if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
            g_day_no++;
        g_day_no += gd;
        int j_day_no = g_day_no - 79;
        int j_np = doubleToInt(j_day_no / 12053);
        j_day_no = j_day_no % 12053;
        int jy = 979 + 33 * j_np + 4 * doubleToInt(j_day_no / 1461);
        j_day_no %= 1461;
        if (j_day_no >= 366) {
            jy += doubleToInt((j_day_no - 1) / 365);
            j_day_no = (j_day_no - 1) % 365;
        }
        for (i = 0; i < 11 && j_day_no >= j_days_in_month[i]; ++i)
            j_day_no -= j_days_in_month[i];
        int jm = i + 1;
        String s = null;
        switch (jm) {
            case 1:
                s = "فروردين";
                break;
            case 2:
                s = "ارديبهشت";
                break;
            case 3:
                s = "خرداد";
                break;
            case 4:
                s = "تير";
                break;
            case 5:
                s = "مرداد";
                break;
            case 6:
                s = "شهريور";
                break;
            case 7:
                s = "مهر";
                break;
            case 8:
                s = "آبان";
                break;
            case 9:
                s = "آذر";
                break;
            case 10:
                s = "دي";
                break;
            case 11:
                s = "بهمن";
                break;
            case 12:
                s = "اسفند";
                break;
        }
        String d = "";
        d += Integer.toString(j_day_no + 1) + " ";
        d += s + " ";
        d += Integer.toString(jy);

        return d;
    }

    public static String gregorian_to_jalali_int(String georgian) {
        String[] splited = georgian.split("-");

        int gy1, gm1, gd1;
        gy1 = Integer.parseInt(splited[0]);
        gm1 = Integer.parseInt(splited[1]);
        gd1 = Integer.parseInt(splited[2]);

        int g_days_in_month[] = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int j_days_in_month[] = new int[]{31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};
        int gy = gy1 - 1600;
        int gm = gm1 - 1;
        int gd = gd1 - 1;
        int g_day_no =
                365 * gy + doubleToInt((gy + 3) / 4) - doubleToInt((gy + 99) / 100) +
                        doubleToInt((gy + 399) / 400);
        int i;
        for (i = 0; i < gm; ++i)
            g_day_no += g_days_in_month[i];
        if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
            g_day_no++;
        g_day_no += gd;
        int j_day_no = g_day_no - 79;
        int j_np = doubleToInt(j_day_no / 12053);
        j_day_no = j_day_no % 12053;
        int jy = 979 + 33 * j_np + 4 * doubleToInt(j_day_no / 1461);
        j_day_no %= 1461;
        if (j_day_no >= 366) {
            jy += doubleToInt((j_day_no - 1) / 365);
            j_day_no = (j_day_no - 1) % 365;
        }
        for (i = 0; i < 11 && j_day_no >= j_days_in_month[i]; ++i)
            j_day_no -= j_days_in_month[i];
        int jm = i + 1;

        String d = "";
        d += Integer.toString(jy) + "/";
        d += jm + "/";
        d += Integer.toString(j_day_no + 1);
        return d;
    }


    public static String humanTimeCreator(String time, Context context) {
        if (time.startsWith("0")) {
            return context.getString(R.string.right_now);
        } else {
            String amount = "";
            String tim = "";
            if (time.contains("minute")) {
                tim = context.getString(R.string.human_date_minute);
                amount = time.substring(0, time.indexOf("minute"));
            }
            if (time.contains("hour")) {
                tim = context.getString(R.string.human_date_hour);
                amount = time.substring(0, time.indexOf("hour"));
            }
            if (time.contains("day")) {
                tim = context.getString(R.string.human_date_day);
                amount = time.substring(0, time.indexOf("day"));
            }
            if (time.contains("week")) {
                tim = context.getString(R.string.human_date_week);
                amount = time.substring(0, time.indexOf("week"));
            }
            if (time.contains("month")) {
                tim = context.getString(R.string.human_date_month);
                amount = time.substring(0, time.indexOf("month"));
            }
            if (time.contains("year")) {
                tim = context.getString(R.string.human_date_year);
                amount = time.substring(0, time.indexOf("year") - 1);
            }
            return (amount + tim);
        }
    }

    private static int doubleToInt(double f) {
        Double fint = new Double(f);
        return fint.intValue();
    }

    public static String getIntervalDate(String startDate, String endDate) {
        String finalDate;
        startDate = JalaliCalendar.gregorian_to_jalali(startDate);
        endDate = JalaliCalendar.gregorian_to_jalali(endDate);
        String[] startDateParts = startDate.split(" ");
        String[] endDateParts = endDate.split(" ");
        if (startDateParts[2].equals(endDateParts[2])) {
            if (startDateParts[1].equals(endDateParts[1])) {
                if (startDateParts[0].equals(endDateParts[0])) {
                    finalDate = endDate;
                } else {
                    finalDate = startDateParts[0] + " تا " + endDate;
                }
            } else {
                finalDate = startDateParts[0] + " " + startDateParts[1] + " تا " + endDate;
            }
        } else {
            finalDate = startDate + " تا " + endDate;
        }
        return finalDate;
    }

    public static int getPersianDay(String day) {
        day = day.toLowerCase();
        if (day.equals("sunday")) {
            return R.string.sunday;
        }
        if (day.equals("monday")) {
            return R.string.monday;
        }
        if (day.equals("tuesday")) {
            return R.string.tuesday;
        }
        if (day.equals("wednesday")) {
            return R.string.wednesday;
        }
        if (day.equals("thursday")) {
            return R.string.thursday;
        }
        if (day.equals("friday")) {
            return R.string.friday;
        }
        if (day.equals("saturday")) {
            return R.string.saturday;
        }
        return -1;

    }

    private static int[] jalaliDays = new int[]{31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};

    private static int fromJalaliStart(int year, int month, int day) {
        int toReturn = 0;
        toReturn += (day - 1);
        for (int i = 0; i < month - 1; i++) {
            toReturn += jalaliDays[i];
        }
        toReturn += 365 * (year - 1390);
        toReturn += ((year - 1388) / 4);
        return toReturn;
    }

    private static int[] gregorianDays = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private static int fromGregorianStart(int year, int month, int day) {
        int toReturn = 286;
        toReturn += (day - 1);
        toReturn += (365) * (year - 2012);
        for (int i = 0; i < month - 1; i++) {
            toReturn += jalaliDays[i];
            if (i == 1 && year % 4 == 0) {
                toReturn++;
            }
        }
        toReturn += ((year) - 2009) / 4;
        return toReturn;
    }

    private static String getJalali(int days) {
        int year = 1390;
        int day = 1;
        int month = 1;
        while (days >= 1461) {
            days -= 1461;
            year += 4;
        }
        if (days >= 365) {
            days -= 365;
            year += 1;
        }
        if (days >= 366) {
            days -= 366;
            year++;
        }
        if (days >= 365) {
            days -= 356;
            year++;
        }
        if (days >= 365) {
            days -= 365;
            year++;
        }
        int m = 0;
        while (days >= jalaliDays[m]) {
            days -= jalaliDays[m];
            month++;
        }
        day += days;
        String toReturn = year + "-";
        if (month < 10) {
            toReturn += "0";
        }
        toReturn += month + "-";
        if (day < 10) {
            toReturn += "0";
        }
        toReturn += day;
        return toReturn;
    }

    private static String getGregorian(int days) {
        days -= 286;
        int year = 2012;
        int day = 1;
        int month = 1;
        int daysOfYear = 366;
        while (days >= daysOfYear) {
            year++;
            days -= daysOfYear;
            if (year % 4 == 0) {
                daysOfYear = 366;
            } else {
                daysOfYear = 365;
            }
        }
        int daysOfMonth = gregorianDays[0];
        while (days >= daysOfMonth) {
            days -= daysOfMonth;
            daysOfMonth = gregorianDays[month];
            if (year % 4 == 0 && month == 1) {
                daysOfMonth++;
            }
            month++;
        }
        day += days;
        String toReturn = year + "-";
        if (month < 10) {
            toReturn += "0";
        }
        toReturn += month + "-";
        if (day < 10) {
            toReturn += "0";
        }
        toReturn += day;
        return toReturn;
    }

    public static String jalaliToGregorian(int year, int month, int day) {
        int days = fromJalaliStart(year, month, day);
        return getGregorian(days);
    }


}
