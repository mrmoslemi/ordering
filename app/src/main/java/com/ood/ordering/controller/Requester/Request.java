package com.ood.ordering.controller.Requester;

import com.loopj.android.http.RequestParams;
import com.ood.ordering.models.Model;

import org.json.JSONObject;

/**
 * Created by Lenovo on 4/3/2017.
 */

public class Request {
    public static final int STATUS_OK = 200;
    public static final int STATUS_CREATED = 201;
    public static final int STATUS_NOT_ACCEPTABLE = 406;

    static final int RESPONSE_NOT_AUTHENTICATED = 401;
    public static final int METHOD_POST = 1;
    public static final int METHOD_GET = 2;
    public static final int STATUS_ACCEPTED = 202;
    private int id;
    private int method;
    private String url;
    private Class<? extends Model> objectsClass;
    private Requester.Waiter waiter;
    private RequestParams params;
    private JSONObject jsonParams;
    private boolean authenticated;

    private static int nextID = 1;


    public Request(int method, String url, Class<? extends Model> objectsClass, Requester.Waiter waiter) {
        this.id = nextID++;
        this.method = method;
        this.url = url;
        this.objectsClass = objectsClass;
        this.waiter = waiter;
        authenticated = true;
    }

    public Request(int method, String url, Class<?extends Model> objectsClass, Requester.Waiter waiter, RequestParams params) {
        this(method, url, objectsClass, waiter);
        this.params = params;
    }

    public Request(int method, String url, Class<?extends Model> objectsClass, Requester.Waiter waiter, JSONObject params) {
        this(method, url, objectsClass, waiter);
        this.jsonParams = params;
    }


    public String getUrl() {
        return url;
    }

    public int getID() {
        return id;
    }

    JSONObject getJsonParams() {
        return jsonParams;
    }

    int getMethod() {
        return method;
    }
    public RequestParams getParams() {
        return params;
    }

    Class<?extends Model> getObjectsClass() {
        return objectsClass;
    }

    boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
    Requester.Waiter getWaiter(){
        return waiter;
    }

    public void setParams(RequestParams params) {
        this.params = params;
    }

    public void setWaiter(Requester.Waiter waiter){
        this.waiter = waiter;
    }
}
