package com.ood.ordering.controller.Requester;

import android.content.Context;

/**
 * Created by Lenovo on 5/21/2017.
 */
public interface RequesterInitializer {
    void authenticationFailure();
    Context getContext();
    boolean usePoolThread();
}