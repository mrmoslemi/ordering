package com.ood.ordering.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.controller.Requester.RequesterInitializer;
import com.ood.ordering.views.baseViews.ToolbarView;
import com.ood.ordering.views.baseViews.MyFragment;
import com.ood.ordering.views.baseViews.TabbedView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener ,RequesterInitializer {
    private View bottomNavigation;

    private ToolbarView toolbar;
    private TabbedView tabbedView;
    private Requester requester;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionGranted();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        tabbedView.handleBack(false);
    }


    @Override
    public void onClick(View v) {
        tabbedView.handleBack(false);
    }

    public void request(Request request) {
        requester.request(request);
    }

    private void permissionGranted() {
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        requester = new Requester();
        requester.initialRequester(this);

        setupUI(findViewById(R.id.main_activity_data_container));

        toolbar = new ToolbarView(findViewById(R.id.toolbar_container), getStatusBarHeight());
        toolbar.setOnBackClickListener(this);

        LinearLayout activityMainLayout = (LinearLayout) findViewById(R.id.activity_main);
        getLayoutInflater().inflate(R.layout.main_page_layout, activityMainLayout);
        bottomNavigation = findViewById(R.id.bottom_navigation_container);
        tabbedView = new TabbedView(this, R.id.content_container, R.id.bottom_navigation_container);


    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void handleBack(boolean shouldFetch) {
        tabbedView.handleBack(shouldFetch);
    }

    public void handleBack() {
        handleBack(false);
    }

    public void setFragment(MyFragment fragment) {
        toolbar.setFragment(fragment);
        if (fragment.hasBottomBar()) {
            bottomNavigation.setVisibility(View.VISIBLE);
        } else {
            bottomNavigation.setVisibility(View.GONE);
        }
    }


    public void setTitle(String title) {
        toolbar.setTitle(title);
    }

    public void setTitle(int titleResID) {
        if (toolbar != null) {
            toolbar.setTitle(titleResID);
        }
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void addFragment(MyFragment fragment) {
        tabbedView.addFragment(fragment);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    public TabbedView getTabbedView() {
        return tabbedView;
    }

    @Override
    public void authenticationFailure() {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean usePoolThread() {
        return false;
    }
    public void handleOSBack() {
        super.onBackPressed();
    }
    public boolean hasBack() {
        return tabbedView.hasBack();
    }
}
