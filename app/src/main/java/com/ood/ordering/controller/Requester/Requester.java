package com.ood.ordering.controller.Requester;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.ood.ordering.models.Model;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Lenovo on 3/17/2017.
 * requester
 */
public class Requester {


    interface Waiter {
        void onFailure(int statusCode, int id);
    }

    public interface ObjectWaiter extends Waiter {
        void onSuccess(int statusCode, int id, Model response);
    }

    public interface ListWaiter extends Waiter {
        void onSuccess(int statusCode, int id, ArrayList<Model> response);
    }

    public interface PaginationListWaiter extends Waiter {
        void onSuccess(int statusCode, int id, ArrayList<Model> response, boolean hasNext, boolean hasPrevious);
    }

    public interface ResponseWaiter extends Waiter {
        void onSuccess(int statusCode, int id);
    }

    private static final String LOCAL_CLIENT_ID = "yl7jQuMAGjvm79SdlYu5Vz92sBILMeil1Yk9BcQj";
    private static final String LOCAL_CLIENT_SECRET = "Rx2XcZGhw3cpBw0msD0Urk3ZO6hzMav87nbTwk6cwqV99UcdXJQmX0HvWoy9ZEU19PV3pB9cXTZbFzxhGxFfWq5Me2jNvUIBijfekcSeiMbmoXQaoQdisEhraJm2WsWL";
    private static final String LOCAL_BASE_URL = "htadasdtp://192.168.43.188:8000/api/v1/";


    private static final String BASE_URL = LOCAL_BASE_URL;

    public static final String URL_CLIENT = "client/";
    public static final String URL_SEARCH = "search/";
    public static final String URL_NEW_PRODUCTS = "new-products/";
    public static final String URL_COMPARE = "compare/";
    public static final String URL_ADD = "add/";
    public static final String URL_REVIEW = "review";



    public static final String client_id = LOCAL_CLIENT_ID;
    public static final String client_secret = LOCAL_CLIENT_SECRET;

    private AsyncHttpClient client = new AsyncHttpClient();
    private final ArrayList<Request> pendingRequests = new ArrayList<>();
    private boolean handlingFailure;
    private RequesterInitializer initializer;


    public void initialRequester(RequesterInitializer initializer) {
        this.initializer = initializer;
    }


    public void request(Request request) {
        Handler handler = new Handler(request, this);
        if (initializer.usePoolThread()) {
            handler.setUsePoolThread(true);
        }
        pendingRequests.add(request);
        switch (request.getMethod()) {
            case Request.METHOD_GET:
                client.get(getUrl(request.getUrl(), true), request.getParams(), handler);
                break;
            case Request.METHOD_POST:
                RequestParams params = request.getParams();
                JSONObject jsonParams = request.getJsonParams();
                if (params != null) {
                    client.post(getUrl(request.getUrl(), true), request.getParams(), handler);
                } else if (jsonParams != null) {
                    StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
                    client.post(initializer.getContext(), getUrl(request.getUrl(), true), entity, "application/json", handler);
                } else {
                    client.post(getUrl(request.getUrl(), true), new RequestParams(), handler);
                }
                break;
        }
    }

    private static String getUrl(String relativeUrl, boolean shouldConcat) {
        if (shouldConcat) {
            return BASE_URL.concat(relativeUrl);
        } else {
            return relativeUrl;
        }
    }

    void success(Request request) {
        pendingRequests.remove(request);
    }

    void authenticationFailure() {
        if (!handlingFailure) {
            handlingFailure = true;
            initializer.authenticationFailure();
        }
    }

    public void setHeader(String token, String tokenType) {
        client.removeAllHeaders();
        client.addHeader("Authorization", tokenType + " " + token);
        handlingFailure = false;
        synchronized (pendingRequests) {
            for (Request request : pendingRequests) {
                request(request);
            }
        }
    }

    public void clearHeader() {
        client.removeAllHeaders();
    }

    boolean isHandlingFailure() {
        return handlingFailure;
    }


    Context getContext() {
        return initializer.getContext();
    }


}
