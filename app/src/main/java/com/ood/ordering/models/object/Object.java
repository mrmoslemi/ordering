package com.ood.ordering.models.object;

import com.ood.ordering.models.Model;

/**
 * Created by Lenovo on 6/8/2017.
 */

class Object implements Model {
    public int price;
    public int quantity;
    public int id;
    public Object(int price,int quantity){
        this.price = price;
        this.quantity = quantity;
    }
}
