package com.ood.ordering.models.person;

/**
 * Created by Lenovo on 6/8/2017.
 */

public class Customer extends Person{
    public int score;
    public Customer(String name, String phoneNumber, String email,int score) {
        super(name, phoneNumber, email);
        this.score = score;
    }
}
