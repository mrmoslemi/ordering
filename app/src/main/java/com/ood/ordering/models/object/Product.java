package com.ood.ordering.models.object;

import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.models.review.Review;

import java.util.List;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class Product extends Object {
    public String name;
    public List<Component> components;
    public List<Review> reviews;
    public List<Compare> compares;
    public boolean isNew;


    public Product(int price, int quantity, String name, List<Component> components,List<Review> reviews,List<Compare> compares, boolean isNew) {
        super(price, quantity);
        this.name = name;
        this.components = components;
        this.reviews = reviews;
        this.compares = compares;
        this.isNew = isNew;

    }


}
