package com.ood.ordering.models.object;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class Component extends Object {
    public String name;
    public String type;

    public Component(int price,int quantity,String name,String type){
        super(price,quantity);
        this.name = name;
        this.type = type;
    }
}
