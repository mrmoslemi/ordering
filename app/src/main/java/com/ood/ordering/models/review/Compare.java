package com.ood.ordering.models.review;

import com.ood.ordering.models.Model;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.models.person.Customer;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class Compare implements Model {
    public Product comparedTo;
    public String context;
    public Customer customer;

    public Compare(Product comparedTo, String context, Customer customer) {
        this.comparedTo = comparedTo;
        this.context = context;
        this.customer = customer;
    }

}
