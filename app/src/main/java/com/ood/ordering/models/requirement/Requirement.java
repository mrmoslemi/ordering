package com.ood.ordering.models.requirement;

import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class Requirement {
    private static List<Requirement> allRequirements = new ArrayList<>();
    private Order order;
    private Component component;
    private boolean isDone;
    private int count;

    public static Requirement createRequirement(Order order, Component component, int count) {
        Requirement newRequirement = new Requirement();
        initialNewRequirement(newRequirement,order, component, count);
        addNewRequirement(newRequirement);
        return newRequirement;
    }

    private static void initialNewRequirement(Requirement newRequirement,Order order, Component component, int count) {
        newRequirement.order = order;
        newRequirement.component = component;
        newRequirement.count = count;
        newRequirement.isDone = false;
    }

    private static void addNewRequirement(Requirement newRequirement) {
        allRequirements.add(newRequirement);
    }
    public void setDone(){
        this.isDone = true;
    }
}
