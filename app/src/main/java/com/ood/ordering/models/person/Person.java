package com.ood.ordering.models.person;

/**
 * Created by Lenovo on 6/8/2017.
 */

class Person {
    public String name;
    public String phoneNumber;
    public String email;


    Person(String name,String phoneNumber,String email){
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}
