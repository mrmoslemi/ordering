package com.ood.ordering.models;


import com.ood.ordering.controller.JalaliCalendar;
import com.ood.ordering.views.baseViews.FormatHelper;

/**
 * Created by Lenovo on 2/26/2017.
 * day
 */

public class Day implements Model {
    public String date;
    public String icon;

    @Override
    public String toString() {
        return FormatHelper.toPersianNumber(JalaliCalendar.gregorian_to_jalali_int(date).substring(2));
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Day)) {
            return other instanceof String && this.date.equals(other);
        } else {
            return this.date.equals(((Day) other).date);
        }
    }

    public int getYear() {
        String persianDate = JalaliCalendar.gregorian_to_jalali_int(date);
        String year = persianDate.substring(0, 4);
        return Integer.parseInt(year);
    }
}
