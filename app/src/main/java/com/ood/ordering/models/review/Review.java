package com.ood.ordering.models.review;

import com.ood.ordering.models.Model;
import com.ood.ordering.models.person.Customer;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class Review implements Model{
    public String textReview;
    public int rate;
    public Customer customer;

    public Review(String textReview,int rate,Customer customer){
        this.textReview = textReview;
        this.rate = rate;
        this.customer = customer;
    }
}
