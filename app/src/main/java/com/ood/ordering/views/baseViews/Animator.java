package com.ood.ordering.views.baseViews;

import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class Animator {
    public static final int ANIMATION_FADE_IN= 1;
    public static final int ANIMATION_FADE_OUT= 2;
    public static final int ANIMATION_ZOOM_IN =3;
    public static final int ANIMATION_ZOOM_OUT =4;
    public static final int ANIMATION_SLIDE_UP =5;
    public static final int ANIMATION_SLIDE_LEFT = 6;

    public static final int TIME_SHORT = 500;
    public static final int DELAY_SHORT = 250;


    public static void animate(View view, int animationType, YoYo.AnimatorCallback onStart, YoYo.AnimatorCallback onEnd, int time){
        Techniques techniques =null;
        switch (animationType){
            case ANIMATION_FADE_IN:
                techniques = Techniques.FadeIn;
                break;
            case ANIMATION_FADE_OUT:
                techniques = Techniques.FadeOut;
                break;
            case ANIMATION_ZOOM_IN:
                techniques = Techniques.BounceIn;
                break;
            case ANIMATION_ZOOM_OUT:
                techniques = Techniques.ZoomOut;
                break;
            case ANIMATION_SLIDE_UP:
                techniques= Techniques.SlideInUp;
                break;
            case ANIMATION_SLIDE_LEFT:
                techniques = Techniques.SlideInLeft;
                break;
        }
        if(onStart==null){
            onStart = new YoYo.AnimatorCallback() {
                @Override
                public void call(android.animation.Animator animator) {

                }
            };
        }
        if(onEnd==null){
            onEnd = new YoYo.AnimatorCallback() {
                @Override
                public void call(android.animation.Animator animator) {

                }
            };
        }

        YoYo.with(techniques).onStart(onStart).onEnd(onEnd).duration(time).playOn(view);
    }
}
