package com.ood.ordering.views.fragments;

import com.loopj.android.http.RequestParams;
import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.views.form.Form;
import com.ood.ordering.views.form.FormFieldView;
import com.ood.ordering.views.form.FormPercentageFieldModel;
import com.ood.ordering.views.form.FormTextFieldView;
import com.ood.ordering.views.fragments.baseFragments.FormFragment;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class ReviewFragment extends FormFragment {
    public static final String PRODUCT = "product";
    private Product product;
    private FormFieldView textField;
    private FormFieldView rateField;
    private String reviewText;
    private int reviewRate;

    @Override
    public void setFields() {
        textField = new FormTextFieldView(getContext(), 3, 150, R.string.review_text, null);
        rateField = new FormPercentageFieldModel(getContext(), R.string.review_rate, 50, 0, 100, "%");
    }

    @Override
    public int getTitle() {
        return R.string.review;
    }

    @Override
    public Request getRequest() {
        RequestParams params = new RequestParams();
        params.put("text", reviewText);
        params.put("product_id", product.id);
        params.put("rate", reviewRate);
        return new Request(Request.METHOD_POST, Requester.URL_REVIEW + Requester.URL_ADD, null, this, params);
    }

    @Override
    public Request getDeleteRequest() {
        return null;
    }

    @Override
    public void validate() {
        if (reviewText == null || reviewText.isEmpty()) {
            textField.addError(Form.NOT_SET);
        }
    }

    @Override
    public int getUpdatedTitle() {
        return 0;
    }

    @Override
    public int getCreatedTitle() {
        return R.string.review_added;
    }

    @Override
    public int getDeleteTitle() {
        return 0;
    }

    @Override
    public int getNotAllowedTitle() {
        return 0;
    }

    @Override
    public void refresh() {

    }

    @Override
    public void notifyChange(FormFieldView field, Serializable newValue) {
        if (field == rateField) {
            reviewRate = (int) newValue;
        } else if (field == textField) {
            reviewText = (String) newValue;
            textField.clearErrors();
        }
    }

    @Override
    protected HashMap<String, Request> getRequests() {
        return null;
    }

    @Override
    public void initialArguments() {
        this.product = (Product) getArguments().getSerializable(PRODUCT);
    }

    @Override
    protected String getLogTag() {
        return "REVIEW ADD";
    }
}
