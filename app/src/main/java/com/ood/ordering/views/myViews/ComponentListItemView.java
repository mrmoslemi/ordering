package com.ood.ordering.views.myViews;

import android.content.Context;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.FormatHelper;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 * view for each component in list
 */
public class ComponentListItemView extends MyView {
    private Component component;

    private TextView nameTextView;
    private HorizontalTitleAndValueView typeTitleAndValue;
    private HorizontalTitleAndValueView priceTitleAndValue;
    private HorizontalTitleAndValueView countTitleAndValue;

    public ComponentListItemView(Context context, Component component) {
        super(context, R.layout.component_list_item_view_layout);
        this.component = component;

        this.nameTextView = (TextView) findViewById(R.id.component_list_item_name);
        this.priceTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.component_list_item_type), R.string.component_type, this.component.type);
        this.priceTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.component_list_item_price), R.string.price, FormatHelper.getPrice(this.component.price));
        this.countTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.component_list_item_count), R.string.count, String.valueOf(this.component.quantity));

        FontHelper.setBold(this.nameTextView);

        this.nameTextView.setText(this.component.name);

    }

    @Override
    public void onDestroy() {
        this.component = null;
        this.nameTextView = null;
        if (this.typeTitleAndValue != null) {
            this.typeTitleAndValue.destroy();
            this.typeTitleAndValue = null;
        }
        if (this.priceTitleAndValue != null) {
            this.priceTitleAndValue.destroy();
            this.priceTitleAndValue = null;
        }
        if (this.countTitleAndValue != null) {
            this.countTitleAndValue.destroy();
            this.countTitleAndValue = null;
        }
    }

}
