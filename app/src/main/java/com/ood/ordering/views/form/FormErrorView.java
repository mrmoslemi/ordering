package com.ood.ordering.views.form;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 */

public class FormErrorView extends MyView {
    public FormErrorView(Context context, String error, ViewGroup parent) {
        super(parent,context, R.layout.form_error_view_layout);
        TextView errorMessage = (TextView) getRootView();
        errorMessage.setText(error);
    }
}
