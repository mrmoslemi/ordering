package com.ood.ordering.views.popups;

import android.graphics.Typeface;
import android.view.View;

import com.ood.ordering.R;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.FontHelper;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/14/2017.
 */
public class SelectPickerPopupView extends SelectPopupView {
    private NumberPicker picker;

    public SelectPickerPopupView(View rootView, ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        super(rootView, choices, defaultValue, listener);
        ArrayList<String> choices1 = new ArrayList<>();
        for (Model model : choices) {
            choices1.add(model.toString());
        }
        picker.setDisplayedValues(choices1.toArray(new String[choices1.size()]));
        picker.setMinValue(0);
        picker.setMaxValue(choices1.size() - 1);
        int defaultValueModel = choices.indexOf(getDefaultValue());
        if (defaultValueModel == -1) {
            defaultValueModel = 0;
        }
        picker.setValue(defaultValueModel);
    }

    @Override
    public int getLayoutID(){
        return R.layout.select_picker_popup_layout;
    }
    @Override
    public void initialViews() {
        picker = (NumberPicker) findViewByID(R.id.select_picker_popup_picker);
        picker.setTextSize(R.dimen.text_size_m);
        picker.setWrapSelectorWheel(false);
        picker.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.NORMAL));


    }

    @Override
    protected void onDestroy() {
        picker = null;
    }

    @Override
    public void onAccept(){
        onSelect(getChoices().get(picker.getValue()));
    }
}
