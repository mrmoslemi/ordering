package com.ood.ordering.views.baseViews;

import com.ood.ordering.views.form.FormTextFieldView;

/**
 * Created by Lenovo on 1/3/2017.
 */

public class FormatHelper {
    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.isEmpty())
            return "";
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else if (c == '٫') {
                out += '،';
            } else {
                out += c;
            }

        }
        return out;
    }

    public static String toEnglishNumber(String text) {
        if (text.isEmpty())
            return "";
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            String c = text.substring(i, i + 1);
            boolean isNumber = false;
            for (int j = 0; j < 10; j++) {
                if (persianNumbers[j].equals(c)) {
                    out += j + "";
                    isNumber = true;
                }
            }
            if (!isNumber) {
                out += c;
            }

        }
        return out;
    }
    public static String toEnglishNumber(String text, int type) {
        if (text == null || text.isEmpty()) {
            if (type == FormTextFieldView.NUMBER) {
                return "0";
            } else {
                return "";
            }
        } else {
            String out = "";
            int length = text.length();
            for (int i = 0; i < length; i++) {
                String c = text.substring(i, i + 1);
                boolean isNumber = false;
                for (int j = 0; j < 10; j++) {
                    if (persianNumbers[j].equals(c)) {
                        out += j + "";
                        isNumber = true;
                    }
                }
                if (!isNumber) {
                    out += c;
                }

            }
            return out;
        }
    }

    public static String getPrice(int price) {
        String toReturn = "";
        while (price > 999) {
            int i = price % 1000;
            String toAdd = ",";
            if (i < 100) {
                toAdd += "0";
            }
            if (i < 10) {
                toAdd += "0";
            }
            toAdd += i;
            toReturn = toAdd + toReturn;
            price -= i;
            price = price / 1000;
        }
        toReturn = price + toReturn;
        toReturn+=" تومان";
        return toReturn;
    }
}