package com.ood.ordering.views.form;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.popups.SelectPopupView;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/14/2017.
 * form select field
 */

abstract class FormSelectFieldView extends FormFieldView implements View.OnClickListener, SelectPopupView.OnSelectListener {
    private boolean editable;
    private int titleResID;
    private ArrayList<Model> choices;
    private Model defaultValue;
    private TextView valueTextView;

    FormSelectFieldView(Context context, boolean editable, int titleResID, ArrayList<Model> choices, Model defaultValue) {
        super(context);
        this.editable = editable;
        this.titleResID = titleResID;
        this.choices = choices;
        this.defaultValue = defaultValue;

    }

    public abstract SelectPopupView getPopupView(ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener);

    @Override
    public View generateFieldView(ViewGroup parent) {
        View toReturn = inflate(R.layout.form_select_field_view,parent);
        valueTextView = (TextView) toReturn.findViewById(R.id.form_select_field_value);
        TextView titleTextView = (TextView) toReturn.findViewById(R.id.form_select_field_title);
        valueTextView.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        titleTextView.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        if (defaultValue == null) {
            valueTextView.setText(R.string.choose);
        } else {
            valueTextView.setText(defaultValue.toString());
        }
        titleTextView.setText(titleResID);
        if (editable) {
            toReturn.setOnClickListener(this);
        }
        return toReturn;
    }

    @Override
    public void onClick(View v) {
        SelectPopupView selectPopupView = getPopupView(choices, defaultValue, this);
        selectPopupView.showAgain();
    }

    @Override
    public void onSelect(Model selected) {
        defaultValue = selected;
        if (defaultValue != null) {
            valueTextView.setText(defaultValue.toString());
        } else {
            valueTextView.setText(R.string.choose);
        }
        setValue(selected);
    }

    public void setChoices(ArrayList<Model> choices) {
        this.choices = choices;
        onSelect(null);
    }
    @Override
    public int getLines() {if (visible) {
        return 1;
    } else {
        return 0;
    }
    }
}
