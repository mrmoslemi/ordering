package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.lang.reflect.Field;

/**
 * Created by Lenovo on 1/3/2017.
 */

public class FontHelper {
    private static FontHelper instance;
    private static Typeface persianTypeface;
    private static Typeface persianBoldTypeface;
    private static Typeface persianLightTypeface;


    private FontHelper(Context context) {
        persianTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans.ttf");
        persianBoldTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Bold.ttf");
        persianLightTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Light.ttf");
    }

    public static synchronized FontHelper getInstance(Context context) {
        if (instance == null)
            instance = new FontHelper(context);
        return instance;
    }

    public Typeface getPersianTextTypeface(int style) {
        if (style == Typeface.BOLD) {
            return persianBoldTypeface;
        }
        if (style == Typeface.ITALIC) {
            return persianLightTypeface;
        }
        return persianTypeface;

    }

    public static void setNumberPickerTextColor(NumberPicker numberPicker) {
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(final NumberPicker view, int scrollState, int a) {
                int count = view.getChildCount();
                for (int i = 0; i < count; i++) {
                    View child = view.getChildAt(i);
                    Log.d("in loop", "counter" + i);
                    if (child instanceof TextView) {
                        Log.d("in if", "counter" + i);
                        try {
                            Field selectorWheelPaintField = view.getClass()
                                    .getDeclaredField("mSelectorWheelPaint");
                            selectorWheelPaintField.setAccessible(true);
                            ((EditText) child).setTypeface(getInstance(child.getContext()).getPersianTextTypeface(0));
                        } catch (NoSuchFieldException e) {
                            Log.w("setNumberPickerTextCor", e);
                        } catch (IllegalArgumentException e) {
                            Log.w("setNumberPickerTtColor", e);
                        }
                    }
                }
            }
        });

    }
    public static void setBold(TextView textView){
        textView.setTypeface(getInstance(textView.getContext()).getPersianTextTypeface(Typeface.BOLD));
    }
}