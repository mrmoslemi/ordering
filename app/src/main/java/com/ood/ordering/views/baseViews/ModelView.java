package com.ood.ordering.views.baseViews;

import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 */

public interface ModelView {
    MyView getView(Model model);
}
