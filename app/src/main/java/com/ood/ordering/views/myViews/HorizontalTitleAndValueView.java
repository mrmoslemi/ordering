package com.ood.ordering.views.myViews;

import android.view.View;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 * view for a title and a value in a row
 */
class HorizontalTitleAndValueView extends MyView {
    private TextView titleTextView;
    private TextView pipeTextView;
    private TextView valueTextView;

    HorizontalTitleAndValueView(View rootView, int titleResID, String value) {
        super(rootView);
        titleTextView = (TextView) findViewById(R.id.horizontal_title_and_value_view_title);
        pipeTextView = (TextView) findViewById(R.id.horizontal_title_and_value_view_pipe);
        valueTextView = (TextView) findViewById(R.id.horizontal_title_and_value_view_value);
        FontHelper.setBold(titleTextView);
        FontHelper.setBold(pipeTextView);
        FontHelper.setBold(valueTextView);

        titleTextView.setText(titleResID);
        valueTextView.setText(value);
    }

    @Override
    public void onDestroy() {
        titleTextView = null;
        pipeTextView = null;
        valueTextView = null;
    }
}
