package com.ood.ordering.views.fragments.baseFragments;

import android.widget.LinearLayout;


import com.ood.ordering.R;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.MyFragment;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.baseViews.ModelView;

import java.util.ArrayList;

/**
 * Created by Lenovo on 3/18/2017.
 * list show fragment
 */
public abstract class ListShowFragment extends MyFragment implements ModelView {
    private LinearLayout list;
    private ArrayList<MyView> myViews;

    @Override
    public int getLayoutID() {
        return R.layout.list_view_layout;
    }

    @Override
    public void initialViews() {
        list = (LinearLayout) findViewById(R.id.list_view_list);
        myViews = new ArrayList<>();
        for (Model model : getModels()) {
            MyView view = getView(model);
            myViews.add(view);
            list.addView(view.getRootView());
        }
    }

    @Override
    public void setListeners() {
    }

    @Override
    public void postActions() {
        getMainActivity().setTitle(getTitle());
    }


    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    public abstract ArrayList<Model> getModels();

    public abstract int getTitle();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (list != null) {
            list.removeAllViews();
            list = null;
        }
        if (myViews != null) {
            for (MyView myView : myViews) {
                myView.destroy();
            }
            myViews.clear();
            myViews = null;
        }

    }


}
