package com.ood.ordering.views.fragments;

import android.os.Bundle;

import com.loopj.android.http.RequestParams;
import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.Model;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.models.person.Customer;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.models.review.Review;
import com.ood.ordering.views.form.Form;
import com.ood.ordering.views.form.FormFieldView;
import com.ood.ordering.views.form.FormTextFieldView;
import com.ood.ordering.views.fragments.baseFragments.FormFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class SearchFragment extends FormFragment {
    private FormTextFieldView productNameField;
    private FormTextFieldView componentNameField;
    private String productName;
    private String componentName;

    @Override
    public void setFields() {
        productNameField = new FormTextFieldView(getContext(), 1, 50, R.string.product_hint, null);
        componentNameField = new FormTextFieldView(getContext(), 1, 50, R.string.component_hint, null);
        addField(productNameField, R.drawable.icon_tab_home);
        addField(componentNameField, R.drawable.icon_tab_home);
    }

    @Override
    public int getTitle() {
        return R.string.tab_search;
    }

    @Override
    public Request getRequest() {
        return null;
    }

    @Override
    public Request getDeleteRequest() {
        return null;
    }

    @Override
    public void validate() {
        if (componentName == null && productName == null) {
            productNameField.addError(Form.NOT_SET);
            componentNameField.addError(Form.NOT_SET);
        } else {
            productNameField.clearErrors();
            componentNameField.clearErrors();
        }
    }

    @Override
    public int getUpdatedTitle() {
        return 0;
    }

    @Override
    public int getCreatedTitle() {
        return 0;
    }

    @Override
    public int getDeleteTitle() {
        return 0;
    }

    @Override
    public int getNotAllowedTitle() {
        return 0;
    }

    @Override
    public void refresh() {

    }

    @Override
    public void notifyChange(FormFieldView field, Serializable newValue) {
        String newString = (String) newValue;
        if (newString.isEmpty()) {
            newString = null;
        }
        if (field == componentNameField) {
            componentName = newString;
            componentNameField.clearErrors();
        } else if (field == productNameField) {
            productName = newString;
            productNameField.clearErrors();
        }
    }

    @Override
    protected HashMap<String, Request> getRequests() {
        return null;
    }

    @Override
    public void initialArguments() {

    }

    @Override
    protected String getLogTag() {
        return "search fragment";
    }

    public void submit() {
        getSubmitButton().setLoading(true);
        int requestCount = 0;

        Request componentRequest = null;
        Request productRequest = null;
        if (componentName != null) {
            requestCount++;
            RequestParams params = new RequestParams();
            params.put("component_name", componentName);
            componentRequest = new Request(Request.METHOD_GET, Requester.URL_SEARCH, Component.class, null, params);
        }
        if (productName != null) {
            requestCount++;
            RequestParams params = new RequestParams();
            params.put("product_name", componentName);
            productRequest = new Request(Request.METHOD_GET, Requester.URL_SEARCH, Product.class, null, params);

        }
        final int finalRequestCount = requestCount;

        final int componentRequestHandlerID = componentRequest == null ? 0 : componentRequest.getID();
        final int productRequestHandlerID = productRequest == null ? 0 : productRequest.getID();

        Requester.ListWaiter waiter = new Requester.ListWaiter() {
            private Bundle bundle;
            private int responseCount = 0;

            @Override
            public void onSuccess(int statusCode, int id, ArrayList<Model> response) {
                log("success" + id);
                if (bundle == null) {
                    bundle = new Bundle();
                }
                responseCount++;
                if (id == componentRequestHandlerID) {
                    bundle.putSerializable(SearchResultFragment.COMPONENTS, response);
                    if (responseCount == finalRequestCount) {
                        showResults();
                    }
                }
                if (id == productRequestHandlerID) {
                    bundle.putSerializable(SearchResultFragment.PRODUCTS, response);
                    if (responseCount == finalRequestCount) {
                        showResults();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, int id) {
                ArrayList<Model> result = new ArrayList<>();
                if (id == componentRequestHandlerID) {
                    for (int i = 0; i < 10; i++) {
                        Component component = new Component((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, "مدل تست " + i);
                        result.add(component);
                    }
                }
                if (id == productRequestHandlerID) {
                    for (int i = 0; i < 10; i++) {
                        List<Component> components = new ArrayList<>();
                        for (int j = 0; j < 5; j++) {
                            Component component = new Component((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, "مدل تست " + i);
                            components.add(component);
                        }
                        List<Review> reviews = new ArrayList<>();
                        for (int j = 0; j < 5; j++) {
                            Customer customer = new Customer("محمد رضا مسلمی", "09350746443", "rmoslemi1994@gmail.com", (j + 1) * 100);
                            Review review = new Review("محصول بسیار خوب و کارایی هست و من به همه چیز امتیاز " + (j + 1) + " میدهم.", j + 1, customer);
                            reviews.add(review);
                        }

                        List<Compare> compares = new ArrayList<>();
                        for (int j = 0; j < i; j++) {
                            Customer customer = new Customer("محمد رضا مسلمی", "09350746443", "rmoslemi1994@gmail.com", (j + 1) * 100);
                            Compare compare = new Compare((Product) result.get(j), "متن تست برای مقایسه بین دو محصول فرضی در سامانه که با توجه به این که در نمایش متن های طولانی چشم نواز تر هستند این متن را طولانی می نویسیم.", customer);
                            compares.add(compare);
                        }
                        Product product = new Product((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, components, reviews, compares, true);
                        result.add(product);
                    }
                }
                onSuccess(statusCode, id, result);
            }

            private void showResults() {
                getSubmitButton().setLoading(false);
                SearchResultFragment fragment = new SearchResultFragment();
                fragment.setArguments(bundle);
                addFragment(fragment);
            }
        };

        if (componentRequest != null) {
            componentRequest.setWaiter(waiter);
            request(componentRequest);
            log("request components");
        }
        if (productRequest != null) {
            productRequest.setWaiter(waiter);
            request(productRequest);
            log("request products");
        }
    }
}
