package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.YoYo;
import com.ood.ordering.controller.MainActivity;
import com.ood.ordering.controller.Requester.Request;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by Lenovo on 1/28/2017.
 * my fragment
 */


public abstract class MyFragment extends Fragment implements Serializable {
    protected View rootView;
    protected Context context;
    protected LayoutInflater inflater;
    protected MainActivity activity;
    protected Bundle savedInstanceState;
    private boolean animated = false;
    private boolean initialized = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        this.savedInstanceState = savedInstanceState;
        this.rootView = inflater.inflate(getLayoutID(), container, false);
        this.context = rootView.getContext();
        this.activity = (MainActivity) getActivity();
        if (!initialized) {
            initialArguments();
            initialized = true;
        }
        initialViews();
        setListeners();
        setFragment();
        postActions();

        return rootView;
    }

    public void doAnimations() {
        if (!animated) {
            animated = true;
            animations();
        }
    }

    protected void animations() {

    }

    protected void setFragment() {
        getMainActivity().setFragment(this);

    }

    public MainActivity getMainActivity() {
        return activity;
    }

    public View getRootView() {
        return this.rootView;
    }

    public View findViewById(int id) {
        return rootView.findViewById(id);
    }

    public Context getContext() {
        return this.context;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public View inflate(int id) {
        View toReturn = inflater.inflate(id, null, false);
        return toReturn;

    }

    public String getStringFromResources(int id) {
        return this.context.getString(id);
    }

    public Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(context, id);
    }

    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    public void addFragment(MyFragment fragment) {
        getMainActivity().addFragment(fragment);
    }

    public abstract int getLayoutID();

    public abstract void initialViews();

    public abstract void setListeners();

    public abstract void postActions();

    public abstract void initialArguments();


    public abstract void itemSelected(int position);

    public abstract ArrayList<String> getOptions();


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onDelete();
    }

    protected void onDelete() {
        rootView = null;
        context = null;
        inflater = null;
        activity = null;
        savedInstanceState = null;
    }

    protected abstract String getLogTag();

    public void log(String log) {
        Log.d(getLogTag(), log);
    }

    public void log(boolean log) {
        Log.d(getLogTag(), String.valueOf(log));
    }

    public void log(Number log) {
        Log.d(getLogTag(), String.valueOf(log));
    }

    public boolean hasBottomBar() {
        return true;
    }

    public boolean hasToolbar() {
        return true;
    }

    public boolean isLocked() {
        return false;
    }

    public void request(Request request) {
        getMainActivity().request(request);
    }

    public boolean toolbarElevation() {
        return true;
    }

    protected int dpToPx(float valueInDp) {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public void animate(final View view, final int animationType, int delay) {
        final YoYo.AnimatorCallback onStart = new YoYo.AnimatorCallback() {
            @Override
            public void call(android.animation.Animator animator) {
                view.setVisibility(View.VISIBLE);
            }
        };
        final YoYo.AnimatorCallback onEnd;
        switch (animationType) {
            case Animator.ANIMATION_FADE_IN:
            case Animator.ANIMATION_ZOOM_IN:
                view.setVisibility(View.INVISIBLE);
                onEnd = null;
                break;
            case Animator.ANIMATION_FADE_OUT:
            case Animator.ANIMATION_ZOOM_OUT:
                onEnd = new YoYo.AnimatorCallback() {
                    @Override
                    public void call(android.animation.Animator animator) {
                        view.setVisibility(View.GONE);
                    }
                };
                break;
            default:
                onEnd = null;
        }
        getRootView().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getRootView() != null) {
                    Animator.animate(view, animationType, onStart, onEnd, Animator.TIME_SHORT);
                }
            }
        }, delay);

    }

    public void animate(View view, int animationType) {
        animate(view, animationType, 0);
    }

    public void animate(MyView view, int animationType) {
        animate(view.getRootView(), animationType);
    }

    public void animate(MyView view, int animationType, int delay) {
        animate(view.getRootView(), animationType, delay);
    }


}
