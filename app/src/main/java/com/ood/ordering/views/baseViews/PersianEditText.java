package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class PersianEditText extends EditText {
    public PersianEditText(Context context) {
        super(context);
        if (!isInEditMode()) {
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(0));
        }
    }

    public PersianEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            int style = getTypeface().getStyle();
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(style));
        }
    }

    public PersianEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            int style = getTypeface().getStyle();
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(style));
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text != null)
            text = FormatHelper.toPersianNumber(text.toString());
        super.setText(text, type);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (getMaxLines() == 1) {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
