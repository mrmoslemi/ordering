package com.ood.ordering.views.myViews;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.FormatHelper;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.fragments.ProductDetailsFragment;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 6/8/2017.
 * view for each product in list
 */
public class ProductListItemView extends MyView  implements View.OnClickListener{
    private Product product;

    private TextView nameTextView;
    private HorizontalTitleAndValueView priceTitleAndValue;
    private HorizontalTitleAndValueView countTitleAndValue;

    private View isNewView;
    private TextView isNewTextView;

    public ProductListItemView(Context context, Product product) {
        super(context, R.layout.product_list_item_view_layout);
        initialViews(product);
    }
    public ProductListItemView(View rootView,Product product){
        super(rootView);
        initialViews(product);
    }
    private void initialViews(Product product){
        this.product = product;
        this.nameTextView = (TextView) findViewById(R.id.product_list_item_name);
        this.priceTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.product_list_item_price), R.string.price, FormatHelper.getPrice(this.product.price));
        this.countTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.product_list_item_count), R.string.count, String.valueOf(this.product.quantity));
        this.isNewView = findViewById(R.id.product_list_item_new);
        this.isNewTextView = (TextView)findViewById(R.id.product_list_item_new_text);

        FontHelper.setBold(this.nameTextView);
        FontHelper.setBold(this.isNewTextView);

        this.nameTextView.setText(this.product.name);
        if(this.product.isNew){
            isNewView.setVisibility(View.VISIBLE);
        }

        setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        this.product = null;
        this.nameTextView = null;
        if (this.priceTitleAndValue != null) {
            this.priceTitleAndValue.destroy();
            this.priceTitleAndValue = null;
        }
        if (this.countTitleAndValue != null) {
            this.countTitleAndValue.destroy();
            this.countTitleAndValue = null;
        }
        isNewView = null;
        isNewTextView = null;
    }

    @Override
    public void onClick(View v) {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ProductDetailsFragment.PRODUCT,this.product);
        fragment.setArguments(bundle);
        getMainActivity().addFragment(fragment);
    }
    public Product getProduct(){
        return product;
    }

    public void setSelected(boolean selected) {
        CircleImageView image = (CircleImageView)findViewById(R.id.product_list_item_image);
        if(selected){
            image.setBorderColor(getContext().getColor(R.color.colorPrimary));
        }else {
            image.setBorderColor(getContext().getColor(R.color.colorAccent));
        }
    }
}
