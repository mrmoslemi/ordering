package com.ood.ordering.views.form;

import android.content.Context;

import com.ood.ordering.models.Day;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.popups.FormSelectDatePopupView;
import com.ood.ordering.views.popups.SelectPopupView;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/15/2017.
 */

public class FormDatePickerFieldView extends FormSelectFieldView {
    public FormDatePickerFieldView(Context context, boolean editable, int titleResID, ArrayList<Model> choices, Day defaultDate) {
        super(context, editable, titleResID, choices, defaultDate);
        setView();
    }


    @Override
    public SelectPopupView getPopupView(ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        return new FormSelectDatePopupView(getRootView(), choices, defaultValue, listener);
    }
}
