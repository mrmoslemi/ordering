package com.ood.ordering.views.fragments;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.Model;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.models.person.Customer;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.models.review.Review;
import com.ood.ordering.views.baseViews.MyFragment;
import com.ood.ordering.views.myViews.ProductListItemView;
import com.ood.ordering.views.myViews.RoundButtonView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class CompareFragment extends MyFragment implements View.OnClickListener {
    public static final String PRODUCT = "product";
    private Product product;
    private Product selectedCompareTo;
    private EditText compareToName;
    private RoundButtonView searchForCompare;
    private LinearLayout searchResultList;
    private EditText compareText;
    private RoundButtonView submit;
    private ArrayList<ProductListItemView> views;

    @Override
    public int getLayoutID() {
        return R.layout.compare_fragment_layout;
    }

    @Override
    public void initialViews() {
        new ProductListItemView(findViewById(R.id.compare_fragment_product), product);
        compareToName = (EditText) findViewById(R.id.compare_fragment_compare_to_name);
        searchForCompare = new RoundButtonView(findViewById(R.id.compare_fragment_search_button), getStringFromResources(R.string.tab_search));
        searchResultList = (LinearLayout) findViewById(R.id.compare_fragment_products_list);
        compareText = (EditText) findViewById(R.id.compare_fragment_compare);
        submit = new RoundButtonView(findViewById(R.id.compare_fragment_submit), getStringFromResources(R.string.submit_compare));
        views = new ArrayList<>();
        compareText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    submit.setEnabled(false);
                }else {
                    if(selectedCompareTo!=null){
                        submit.setEnabled(true);
                    }else {
                        submit.setEnabled(false);
                    }
                }
            }
        });
    }

    @Override
    public void setListeners() {
        submit.setOnClickListener(this);
        searchForCompare.setOnClickListener(this);
        submit.setEnabled(false);
    }

    @Override
    public void postActions() {
        getMainActivity().setTitle(R.string.compare);
    }

    @Override
    public void initialArguments() {
        product = (Product) getArguments().getSerializable(PRODUCT);
    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    @Override
    protected String getLogTag() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if (v == submit.getRootView()) {
            submitCompare();
        } else if (v == searchForCompare.getRootView()) {
            searchForItems();
        }
    }

    private void searchForItems() {
        searchForCompare.setLoading(true);
        String text = compareToName.getText().toString();
        RequestParams params = new RequestParams();
        params.put("name",text);
        Request request = new Request(Request.METHOD_GET, Requester.URL_SEARCH, Product.class, new Requester.ListWaiter() {
            @Override
            public void onSuccess(int statusCode, int id, ArrayList<Model> response) {
                searchResultList.removeAllViews();
                selectedCompareTo = null;
                submit.setEnabled(false);
                searchForCompare.setLoading(false);
                views.clear();
                for(Model model:response){
                    final ProductListItemView view = new ProductListItemView(getContext(),(Product)model);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedCompareTo = view.getProduct();
                            if(!compareText.getText().toString().isEmpty()){
                                submit.setEnabled(true);
                            }
                            for(ProductListItemView view1:views){
                                view1.setSelected(false);
                            }
                            view.setSelected(true);
                        }
                    });
                    views.add(view);
                    searchResultList.addView(view.getRootView());
                }
            }

            @Override
            public void onFailure(int statusCode, int id) {
                ArrayList<Model> result = new ArrayList<>();
                for (int i = 0; i < 10; i++) {
                    List<Component> components = new ArrayList<>();
                    for (int j = 0; j < 5; j++) {
                        Component component = new Component((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, "مدل تست " + i);
                        components.add(component);
                    }
                    List<Review> reviews = new ArrayList<>();
                    for (int j = 0; j < 5; j++) {
                        Customer customer = new Customer("محمد رضا مسلمی", "09350746443", "rmoslemi1994@gmail.com", (j + 1) * 100);
                        Review review = new Review("محصول بسیار خوب و کارایی هست و من به همه چیز امتیاز " + (j + 1) + " میدهم.", j + 1, customer);
                        reviews.add(review);
                    }

                    List<Compare> compares = new ArrayList<>();
                    for (int j = 0; j < i; j++) {
                        Customer customer = new Customer("محمد رضا مسلمی", "09350746443", "rmoslemi1994@gmail.com", (j + 1) * 100);
                        Compare compare = new Compare((Product) result.get(j), "متن تست برای مقایسه بین دو محصول فرضی در سامانه که با توجه به این که در نمایش متن های طولانی چشم نواز تر هستند این متن را طولانی می نویسیم.", customer);
                        compares.add(compare);
                    }
                    Product product = new Product((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, components, reviews, compares, true);
                    result.add(product);
                }
                onSuccess(statusCode, id, result);
            }
        });
        request(request);
    }
    private void submitCompare(){
        RequestParams params = new RequestParams();
        params.put("product_id_1",product.id);
        params.put("product_id_2",selectedCompareTo.id);
        params.put("compare",compareText.getText().toString());
        Request request = new Request(Request.METHOD_POST, Requester.URL_COMPARE+Requester.URL_ADD, null, new Requester.ResponseWaiter() {
            @Override
            public void onSuccess(int statusCode, int id) {
                getMainActivity().handleBack(true);
                Toast.makeText(getContext(),R.string.compare_added,Toast.LENGTH_LONG).show();;
            }

            @Override
            public void onFailure(int statusCode, int id) {
                onSuccess(200,id);
            }
        },params);
        request(request);

    }
}
