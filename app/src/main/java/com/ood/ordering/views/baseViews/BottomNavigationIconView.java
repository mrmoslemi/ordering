package com.ood.ordering.views.baseViews;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ood.ordering.R;

/**
 * Created by Lenovo on 5/25/2017.
 */
public class BottomNavigationIconView extends MyView {
    private ImageView icon;
    private TextView text;

    public BottomNavigationIconView(View rootView, int drawable, int textResID) {
        super(rootView);
        this.icon = (ImageView) findViewById(R.id.bottom_navigation_icon_image);
        this.text = (TextView) findViewById(R.id.bottom_navigation_icon_title);
        icon.setImageDrawable(getDrawable(drawable));

        FontHelper.setBold(text);
        text.setText(textResID);

        icon.setColorFilter(new PorterDuffColorFilter(icon.getContext().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP));
    }

    public void setColored(boolean colored) {
        if (colored) {
            getRootView().setAlpha(1f);
        } else {
            getRootView().setAlpha(0.2f);
        }
    }

    @Override
    public void onDestroy() {
        icon = null;
        text = null;
    }
}

