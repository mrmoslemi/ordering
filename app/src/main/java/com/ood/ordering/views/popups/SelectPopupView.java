package com.ood.ordering.views.popups;

import android.view.View;

import com.ood.ordering.R;
import com.ood.ordering.models.Model;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/14/2017.
 * select popup view
 */
public abstract class SelectPopupView extends PopupView {
    private OnSelectListener listener;
    private ArrayList<Model> choices;
    private Model defaultValue;

    public interface OnSelectListener {
        void onSelect(Model selected);
    }
    public interface CheckableView {
        void setOnClickListener(View.OnClickListener listener);

        void setChecked(boolean checked);

        View getRootView();
    }


    public SelectPopupView(View container, ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        super(container);
        this.choices = choices;
        this.defaultValue = defaultValue;
        this.listener = listener;
    }

    @Override
    public int getTitleResID() {
        return R.string.choose;
    }

    @Override
    public int getHeaderImageID() {
        return 0;
    }

    public Model getDefaultValue() {
        return defaultValue;
    }

    public ArrayList<Model> getChoices() {
        return choices;
    }

    public void onSelect(Model model) {
        listener.onSelect(model);
        defaultValue = model;
        dismiss();
    }


}
