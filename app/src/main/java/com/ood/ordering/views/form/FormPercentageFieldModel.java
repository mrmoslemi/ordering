package com.ood.ordering.views.form;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;


/**
 * Created by Lenovo on 3/14/2017.
 * form percentage field
 */
public class FormPercentageFieldModel extends FormFieldView implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private int titleResID;
    private int defaultPercentage;
    private SeekBar seekbar;
    private TextView value;
    private TextView title;
    private View toReturn;
    private int min;
    private int max;
    private String desc;

    public FormPercentageFieldModel(Context context, int titleResID, int defaultPercentage, int min, int max, String desc) {
        super(context);
        this.titleResID = titleResID;
        this.defaultPercentage = defaultPercentage;
        this.min = min;
        this.max = max;
        this.desc = desc;
        setView();
    }

    @Override
    public View generateFieldView(ViewGroup parent) {
        toReturn = inflate(R.layout.form_percentage_field_view_layout, parent);
        title = (TextView) toReturn.findViewById(R.id.form_percentage_title);
        title.setText(titleResID);
        value = (TextView) toReturn.findViewById(R.id.form_percentage_value);
        value.setText(String.valueOf(defaultPercentage).concat(" "+desc));
        seekbar = (SeekBar) toReturn.findViewById(R.id.form_percentage_seek_bar);
        title.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        value.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        showSeekBar(false);
        return toReturn;
    }

    @Override
    public void onClick(View v) {
        showSeekBar(true);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int percentage = getPercentage(progress);
        value.setText(String.valueOf(percentage).concat(" " + desc));
        setValue(percentage);
    }

    private int getPercentage(int progress) {
        double percentage = (double) (progress * (max - min)) / 100.0;
        return min + (int) percentage;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void showSeekBar(boolean show) {
        if (show) {
            toReturn.setOnClickListener(null);
            title.setVisibility(View.INVISIBLE);
            seekbar.setVisibility(View.VISIBLE);
            seekbar.setOnSeekBarChangeListener(this);
        } else {
            title.setVisibility(View.VISIBLE);
            seekbar.setVisibility(View.INVISIBLE);
            toReturn.setOnClickListener(this);
            seekbar.setOnSeekBarChangeListener(null);
        }
    }

    @Override
    public int getLines() {
        if (visible) {
            return 1;
        } else {
            return 0;
        }
    }
}
