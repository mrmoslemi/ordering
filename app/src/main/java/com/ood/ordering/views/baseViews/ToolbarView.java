package com.ood.ordering.views.baseViews;

import android.graphics.Typeface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ood.ordering.R;

import java.util.ArrayList;

/**
 * Created by Lenovo on 3/17/2017.
 * new toolbar
 */

public class ToolbarView extends MyView implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String ADD = "add";
    public static final String EDIT = "edit";
    public static final String SAVE = "save";


    private TextView title;
    private View backButton;
    private View actionsButton;
    private Button actionButton;
    private RelativeLayout anchor;
    private MyFragment holdingFragment;
    private ListPopupWindow popupWindow;
    private ProgressBar loading;

    public ToolbarView(View rootView, int paddinTop) {
        super(rootView);
        rootView.setPadding(0, paddinTop, 0, 0);

        title = (TextView) findViewById(R.id.toolbar_title);
        title.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        backButton = findViewById(R.id.toolbar_back_button);
        actionsButton = findViewById(R.id.toolbar_options_button);
        actionButton = (Button) findViewById(R.id.toolbar_action_button);
        loading = (ProgressBar) findViewById(R.id.toolbar_action_loading);
        anchor = (RelativeLayout) findViewById(R.id.toolbar_anchor);
        actionsButton.setOnClickListener(this);
        actionButton.setOnClickListener(this);
    }

    public void setOnBackClickListener(View.OnClickListener listener) {
        backButton.setOnClickListener(listener);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setTitle(int titleResID) {
        this.title.setText(titleResID);
    }

    public void setFragment(MyFragment fragment) {
        if (!fragment.hasToolbar()) {
            setVisible(false);
        } else {
            setVisible(true);
            holdingFragment = fragment;
            ArrayList<String> actions = fragment.getOptions();
            if (getMainActivity().hasBack()) {
                this.backButton.setVisibility(View.VISIBLE);
            } else {
                this.backButton.setVisibility(View.INVISIBLE);
            }
            this.loading.setVisibility(View.GONE);
            if (actions == null) {
                this.actionsButton.setVisibility(View.GONE);
                this.actionButton.setVisibility(View.GONE);

            } else {
                int sizeOfActions = actions.size();
                if (sizeOfActions == 1) {
                    this.actionsButton.setVisibility(View.GONE);
                    this.actionButton.setVisibility(View.VISIBLE);
                    String action = actions.get(0);
                    if (action.equals(ADD)) {
                        actionButton.setText(getString(R.string.add));
                    } else if (action.equals(EDIT)) {
                        actionButton.setText(getString(R.string.edit));
                    } else if (action.equals(SAVE)) {
                        actionButton.setText(getString(R.string.save));
                    }
                } else {
                    this.actionsButton.setVisibility(View.VISIBLE);
                    this.actionButton.setVisibility(View.GONE);
                    popupWindow = new ListPopupWindow(getContext());
                    anchor.removeAllViews();
                    for (String string : actions) {
                        PersianTextView persianTextView = new PersianTextView(getContext());
                        persianTextView.setText(string);
                        persianTextView.setLines(1);
                        persianTextView.setMaxLines(1);
                        int padding = (int) getContext().getResources().getDimension(R.dimen.padding_s);
                        persianTextView.setPadding(padding, padding, padding, padding);
                        persianTextView.setVisibility(View.INVISIBLE);
                        anchor.addView(persianTextView);
                    }
                    popupWindow.setAnchorView(anchor);
                    popupWindow.setContentWidth(ListPopupWindow.WRAP_CONTENT);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.drop_down_item_layout, R.id.drop_down_item_text, actions);
                    popupWindow.setAdapter(adapter);
                    popupWindow.setOnItemClickListener(this);
                }


            }
        }


    }


    @Override
    public void onClick(View v) {
        if (v == actionsButton) {
            popupWindow.show();
        } else if (v == actionButton) {
            holdingFragment.itemSelected(0);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        holdingFragment.itemSelected(position);
        popupWindow.dismiss();
    }

    public void setLoading(boolean loading) {
        if (actionButton.getVisibility() == View.VISIBLE || this.loading.getVisibility() == View.VISIBLE) {
            if (loading) {
                actionButton.setVisibility(View.GONE);
                this.loading.setVisibility(View.VISIBLE);
            } else {
                actionButton.setVisibility(View.VISIBLE);
                this.loading.setVisibility(View.GONE);
            }
        }
    }

}