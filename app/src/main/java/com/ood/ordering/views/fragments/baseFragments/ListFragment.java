package com.ood.ordering.views.fragments.baseFragments;

import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.baseViews.ModelView;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Lenovo on 6/6/2017.
 */

public abstract class ListFragment extends QueryFragment implements ModelView {
    private static final String LIST = "list";
    private LinearLayout list;
    private ArrayList<MyView> myViews;

    @Override
    public int getContentLayoutID() {
        return R.layout.list_view_layout;
    }

    @Override
    protected void finishedLoading() {
        myViews = new ArrayList<>();
        list = (LinearLayout) findViewById(R.id.list_view_list);
        ArrayList<Model> models = getList(LIST).first;
        for (Model model : models) {
            MyView view = getView(model);
            myViews.add(view);
            list.addView(view.getRootView());
        }
    }

    @Override
    protected HashMap<String, Request> getRequests() {
        Request request = new Request(Request.METHOD_GET, getUrl(), getObjectsClass(), this);
        HashMap<String, Request> toReturn = new HashMap<>();
        toReturn.put(LIST, request);
        return toReturn;
    }



    public abstract Class<? extends Model> getObjectsClass();

    public abstract String getUrl();


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (list != null) {
            list.removeAllViews();
            list = null;
        }
        if (myViews != null) {
            for (MyView myView : myViews) {
                myView.destroy();
            }
            myViews.clear();
            myViews = null;
        }
    }
}
