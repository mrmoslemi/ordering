package com.ood.ordering.views.baseViews;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.controller.MainActivity;
import com.ood.ordering.views.fragments.NewsFragment;
import com.ood.ordering.views.fragments.OrderingFragment;
import com.ood.ordering.views.fragments.ProfileFragment;
import com.ood.ordering.views.fragments.SearchFragment;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class TabbedView {
    private static final int DEFAULT_FRAGMENT = 0;
    private Activity context;
    private int containerLayoutID;
    private LinearLayout linearLayout;
    private ArrayList<ArrayList<MyFragment>> appFragments;
    private ArrayList<Integer> interFragmentStack;
    private ArrayList<BottomNavigationIconView> bottomNavigationIconViews;
    private static final int DIRECTION_FORWARD = 1;
    private static final int DIRECTION_BACKWARD = 2;
    private static final int DIRECTION_TELEP = 3;


    public TabbedView(Activity context, int containerLayoutID, int bottomBarLayoutID) {
        this.containerLayoutID = containerLayoutID;
        this.context = context;
        this.linearLayout = (LinearLayout) context.findViewById(bottomBarLayoutID);
        appFragments = new ArrayList<>();
        interFragmentStack = new ArrayList<>();
        init();
    }

    private void init() {
        bottomNavigationIconViews = new ArrayList<>();
        bottomNavigationIconViews.add(new BottomNavigationIconView(linearLayout.getChildAt(0), R.drawable.icon_tab_home, R.string.tab_search));
        bottomNavigationIconViews.add(new BottomNavigationIconView(linearLayout.getChildAt(1), R.drawable.icon_tab_home, R.string.tab_news));
        bottomNavigationIconViews.add(new BottomNavigationIconView(linearLayout.getChildAt(2), R.drawable.icon_tab_home, R.string.tab_order));
        bottomNavigationIconViews.add(new BottomNavigationIconView(linearLayout.getChildAt(3), R.drawable.icon_tab_home, R.string.tab_profile));

        for (int i = 0; i < bottomNavigationIconViews.size(); i++) {
            final int index = i;
            bottomNavigationIconViews.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setFragment(index);
                }
            });
        }
        for (int i = 0; i < 5; i++) {
            appFragments.add(new ArrayList<MyFragment>());
        }
        setFragment(DEFAULT_FRAGMENT);

    }


    private void setFragment(int i) {
        int interFragmentStackSize = interFragmentStack.size();
        boolean isLocked = false;
        if (interFragmentStackSize != 0) {
            ArrayList<MyFragment> currentFragmentStack = appFragments.get(interFragmentStack.get(interFragmentStack.size() - 1));
            MyFragment currentFragment = currentFragmentStack.get(currentFragmentStack.size() - 1);
            isLocked = currentFragment.isLocked();
        }
        if (!isLocked) {
            for (int j = 0; j < bottomNavigationIconViews.size(); j++) {
                bottomNavigationIconViews.get(j).setColored(i == j);
            }
            ArrayList<MyFragment> thisFragmentStack = appFragments.get(i);
            if (thisFragmentStack.isEmpty()) {
                MyFragment toPlace = getFragment(i);
                thisFragmentStack.add(toPlace);
                interFragmentStack.remove(Integer.valueOf(i));
                interFragmentStack.add(i);

            } else {
                if (interFragmentStack.get(interFragmentStack.size() - 1) == i) {
                    if (thisFragmentStack.size() > 1) {
                        thisFragmentStack.clear();
                        MyFragment toPlace = getFragment(i);
                        thisFragmentStack.add(toPlace);
                    }
                } else {
                    interFragmentStack.remove(Integer.valueOf(i));
                    interFragmentStack.add(i);
                }
            }
            MyFragment toSwitchTo = thisFragmentStack.get(thisFragmentStack.size() - 1);
            fragmentTrans(toSwitchTo, DIRECTION_TELEP);
        }
    }

    public void handleBack() {
        handleBack(false);
    }

    public void handleBack(boolean refresh) {
        int currentFragmentNumber = interFragmentStack.get(interFragmentStack.size() - 1);
        ArrayList<MyFragment> currentFragmentStack = appFragments.get(currentFragmentNumber);
        if (!currentFragmentStack.get(currentFragmentStack.size() - 1).isLocked()) {
            if (currentFragmentStack.size() == 1) {
                if (interFragmentStack.size() != 1) {
                    currentFragmentStack.remove(0);
                    interFragmentStack.remove(interFragmentStack.size() - 1);
                    int toSwtichToFragmentNumber = interFragmentStack.get(interFragmentStack.size() - 1);
                    for (int j = 0; j < bottomNavigationIconViews.size(); j++) {
                        bottomNavigationIconViews.get(j).setColored(toSwtichToFragmentNumber == j);
                    }
                    ArrayList<MyFragment> toSwitchToFragmentStack = appFragments.get(toSwtichToFragmentNumber);

                    MyFragment toSwitchTo = toSwitchToFragmentStack.get(toSwitchToFragmentStack.size() - 1);
                    if (refresh) {
                        try {
                            Field field = toSwitchTo.getClass().getField("shouldFetch");
                            field.set(toSwitchTo, true);
                        } catch (NoSuchFieldException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                    fragmentTrans(toSwitchTo, DIRECTION_TELEP);
                } else {
                    ((MainActivity) context).handleOSBack();
                }
            } else {
                currentFragmentStack.remove(currentFragmentStack.size() - 1);
                MyFragment toSwitchTo = currentFragmentStack.get(currentFragmentStack.size() - 1);
                if (refresh) {
                    try {
                        Field field = toSwitchTo.getClass().getField("shouldFetch");
                        field.set(toSwitchTo, true);
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                fragmentTrans(toSwitchTo, DIRECTION_BACKWARD);
            }
        } else {
            ((MainActivity) context).handleOSBack();
        }
    }

    public void addFragment(MyFragment fragment) {
        int currentFragmentNumber = interFragmentStack.get(interFragmentStack.size() - 1);
        ArrayList<MyFragment> currentFragmentStack = appFragments.get(currentFragmentNumber);
        currentFragmentStack.add(fragment);
        fragmentTrans(fragment, DIRECTION_FORWARD);

    }

    private void fragmentTrans(MyFragment toSwitchTo, int direction) {
        if (toSwitchTo.hasBottomBar()) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
        }
        FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (direction == DIRECTION_FORWARD) {
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        } else if (direction == DIRECTION_BACKWARD) {
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        }
        fragmentTransaction.replace(containerLayoutID, toSwitchTo);
        fragmentTransaction.commit();
    }

    private MyFragment getFragment(int i) {
        switch (i) {
            case 0:
                return new SearchFragment();
            case 1:
                return new NewsFragment();
            case 2:
                return new OrderingFragment();
            case 3:
                return new ProfileFragment();
            default:
                return null;
        }
    }

    public boolean hasBack() {
        int topStack = interFragmentStack.get(interFragmentStack.size() - 1);
        ArrayList<MyFragment> fragmentStack = appFragments.get(topStack);
        return fragmentStack.size() > 1;
    }

    public void clear() {
        interFragmentStack = new ArrayList<>();
        appFragments = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            appFragments.add(new ArrayList<MyFragment>());
        }
        setFragment(DEFAULT_FRAGMENT);
    }

}
