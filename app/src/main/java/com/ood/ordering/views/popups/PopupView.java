package com.ood.ordering.views.popups;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.transition.Fade;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.controller.MainActivity;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.views.baseViews.FontHelper;


/**
 * Created by Lenovo on 1/28/2017.
 * base popup view
 */

public abstract class PopupView {
    private View container;
    private View rootView;
    private MyPopupWindow popupWindow;
    private LayoutInflater inflater;
    private Context context;
    private TextView cancel;
    private TextView accept;
    private View frameView;
    private ImageView imageView;
    private TextView headerText;
    private LinearLayout rootContainer;

    public PopupView(View container) {
        this.context = container.getContext();
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.container = container;
        this.frameView = inflater.inflate(R.layout.base_popup_with_header_layout, null, false);
        this.imageView = (ImageView) frameView.findViewById(R.id.base_popup_image);
        int imageResID = getHeaderImageID();
        if (imageResID != 0) {
            this.imageView.setImageDrawable(getDrawable(imageResID));
        }
        this.headerText = (TextView) frameView.findViewById(R.id.base_popup_title_text);
        int titleResID = getTitleResID();
        if (titleResID == 0) {
            this.headerText.setVisibility(View.GONE);
        } else {
            this.headerText.setText(titleResID);
        }
        FontHelper.setBold(headerText);
        this.rootContainer = (LinearLayout) frameView.findViewById(R.id.base_popup_root_container);
        this.rootView = inflate(getLayoutID());

        this.rootContainer.removeAllViews();
        this.rootContainer.addView(rootView);

        this.accept = (TextView) frameView.findViewById(R.id.base_popup_accept_button);
        this.cancel = (TextView) frameView.findViewById(R.id.base_popup_cancel_button);
        FontHelper.setBold(accept);
        FontHelper.setBold(cancel);
        this.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAccept();
            }
        });
        this.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancel();
            }
        });
        this.popupWindow = new MyPopupWindow(frameView);
        this.popupWindow.setWidth(ListPopupWindow.MATCH_PARENT);
        this.popupWindow.setHeight(ListPopupWindow.MATCH_PARENT);
        this.popupWindow.setFocusable(true);
        this.popupWindow.setExitTransition(new Fade());
        this.popupWindow.setEnterTransition(new Fade());
        initialViews();

    }

    protected abstract int getLayoutID();

    protected abstract void onAccept();

    private void onCancel() {
        dismiss();
    }

    protected abstract void initialViews();

    protected abstract int getHeaderImageID();

    public abstract int getTitleResID();

    protected TextView getAccept() {
        return accept;
    }

    protected TextView getCancel() {
        return cancel;
    }


    public void showAgain() {
        popupWindow.showAtLocation(container, Gravity.CENTER, 0, 0);
    }

    public void dismiss() {
        popupWindow.dismiss();
        destroy();
    }

    public Context getContext() {
        return this.context;
    }

    private View inflate(int id) {
        return inflater.inflate(id, null, false);
    }

    public String getString(int id) {
        return context.getString(id);
    }

    public Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(getContext(), id);
    }

    public void request(Request request) {
        getMainActivity().request(request);
    }

    public MainActivity getMainActivity() {
        return (MainActivity) context;
    }

//    public void setDismissible(boolean dismissible) {
//        popupWindow.setDismissible(dismissible);
//    }

    protected View findViewByID(int id) {
        return rootView.findViewById(id);
    }

    protected abstract void onDestroy();

    private void destroy() {
        onDestroy();
        container = null;
        rootView = null;
        popupWindow = null;
        inflater = null;
        context = null;
        cancel = null;
        accept = null;
        frameView = null;
        imageView = null;
        headerText = null;
        rootContainer = null;
    }
}


class MyPopupWindow extends PopupWindow {
    private boolean dismissible;

    @Override
    public void dismiss() {
        if (dismissible) {
            super.dismiss();
        }
    }

    MyPopupWindow(View rootView) {
        super(rootView);
        this.dismissible = true;
    }

//    void setDismissible(boolean dismissible) {
//        this.dismissible = dismissible;
//    }
}
