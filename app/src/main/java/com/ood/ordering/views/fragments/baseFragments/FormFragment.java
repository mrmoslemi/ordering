package com.ood.ordering.views.fragments.baseFragments;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.form.Form;
import com.ood.ordering.views.form.FormFieldView;
import com.ood.ordering.views.myViews.RoundButtonView;

import java.util.ArrayList;


/**
 * Created by Lenovo on 4/15/2017.
 * form fragment
 */

public abstract class FormFragment extends QueryFragment implements Form.FormHandler, View.OnClickListener {
    private Form form;
    private LinearLayout list;

    @Override
    public int getContentLayoutID() {
        return R.layout.form_fragment_layout;
    }

    public void setButtonsContainerVisible() {
        form.setButtonsContainerVisible();
    }

    @Override
    protected void finishedLoading() {
        this.list = (LinearLayout) findViewById(R.id.form_list);
        setForm(new Form(getContext(), this, getTitle()));
        getRootView().post(new Runnable() {
            @Override
            public void run() {
                setFields();
                doAnimations();
            }
        });

        getMainActivity().setTitle(getTitle());
    }

    @Override
    public void animations() {
        form.doAnimations(0);
    }

    @Override
    public void postActions() {

    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    public void setForm(Form form) {
        if (this.form != null) {
            list.removeView(this.form.getRootView());
        }
        this.form = form;
        list.addView(this.form.getRootView());
        form.getSubmitButton().setOnClickListener(this);
        form.getDeleteButton().setOnClickListener(this);
        form.getRefreshButton().setOnClickListener(this);
    }

    public void addField(FormFieldView formFieldModel, int titleRes) {
        form.addField(formFieldModel, titleRes);
    }

    public LinearLayout getList() {
        return list;
    }

    public void submit() {
        Request submitRequest = getRequest();
        form.getSubmitButton().setLoading(true);
        submitRequest.setWaiter(new Requester.ResponseWaiter() {
            @Override
            public void onSuccess(int statusCode, int id) {
                form.getSubmitButton().setLoading(false);
                if (statusCode == Request.STATUS_OK) {
                    Toast toast = Toast.makeText(getContext(), getUpdatedTitle(), Toast.LENGTH_SHORT);
                    toast.show();
                    getMainActivity().handleBack(true);
                } else if (statusCode == Request.STATUS_CREATED) {
                    Toast toast = Toast.makeText(getContext(), getCreatedTitle(), Toast.LENGTH_SHORT);
                    toast.show();
                    getMainActivity().handleBack(true);
                } else if (statusCode == Request.STATUS_NOT_ACCEPTABLE) {
                    Toast toast = Toast.makeText(getContext(), getNotAllowedTitle(), Toast.LENGTH_SHORT);
                    toast.show();
                    getMainActivity().handleBack(true);
                } else {
                    Toast toast = Toast.makeText(getContext(), R.string.failed, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            @Override
            public void onFailure(int statusCode, int id) {
                onSuccess(statusCode, id);
            }
        });
        request(submitRequest);
    }

    public Form getForm() {
        return form;
    }
    public RoundButtonView getSubmitButton(){
        return form.getSubmitButton();
    }

    @Override
    public void onClick(View v) {
        if (v == form.getSubmitButton().getRootView()) {
            validate();
            if (form.isValid()) {
                submit();
            } else {
                Toast toast = Toast.makeText(getContext(), R.string.error_form, Toast.LENGTH_SHORT);
                toast.show();
            }
        } else if (v == form.getDeleteButton().getRootView()) {
            Request deleteRequest = getDeleteRequest();
            form.getDeleteButton().setLoading(true);
            deleteRequest.setWaiter(new Requester.ResponseWaiter() {
                @Override
                public void onSuccess(int statusCode, int id) {
                    form.getDeleteButton().setLoading(false);
                    if (statusCode == Request.STATUS_NOT_ACCEPTABLE) {
                        Toast toast = Toast.makeText(getContext(), getNotAllowedTitle(), Toast.LENGTH_SHORT);
                        toast.show();
                        getMainActivity().handleBack(true);
                    } else if (statusCode == Request.STATUS_OK) {
                        Toast toast = Toast.makeText(getContext(), getDeleteTitle(), Toast.LENGTH_SHORT);
                        toast.show();
                        getMainActivity().handleBack(true);
                    } else {
                        Toast toast = Toast.makeText(getContext(), R.string.failed, Toast.LENGTH_SHORT);
                        toast.show();
                    }

                }

                @Override
                public void onFailure(int statusCode, int id) {
                    onSuccess(statusCode, id);
                }
            });
            request(deleteRequest);
        } else if (v == form.getRefreshButton().getRootView()) {
            refresh();
        }
    }

    public abstract void setFields();

    public abstract int getTitle();

    public abstract Request getRequest();

    public abstract Request getDeleteRequest();

    public abstract void validate();

    public abstract int getUpdatedTitle();

    public abstract int getCreatedTitle();

    public abstract int getDeleteTitle();

    public abstract int getNotAllowedTitle();

    public abstract void refresh();

    public ArrayList<Model> getModelList(String tag) {
        return getList(tag).first;
    }

    public Model getModel(String tag) {
        return getObject(tag).first;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (form != null) {
            form.destroy();
            form = null;
        }
        list = null;
    }
}
