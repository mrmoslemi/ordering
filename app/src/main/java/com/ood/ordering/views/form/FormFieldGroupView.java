package com.ood.ordering.views.form;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.Animator;
import com.ood.ordering.views.baseViews.MyView;

import java.util.ArrayList;


/**
 * Created by Lenovo on 2/26/2017.
 * form group
 */
class FormFieldGroupView extends MyView {
    private RelativeLayout fieldsContainer;
    private ArrayList<FormFieldView> fields;
    private int lines = 0;
    private Form form;

    FormFieldGroupView(Context context, int iconResID) {
        super(context, R.layout.form_group_field_view_layout);
        ImageView icon = (ImageView) findViewById(R.id.form_field_view_caption);
        icon.setImageDrawable(getDrawable(iconResID));
        fields = new ArrayList<>();
        fieldsContainer = (RelativeLayout) findViewById(R.id.form_group_fields_container);
    }

    public void addField(FormFieldView formFieldModel) {
        formFieldModel.setGroup(this);
        fields.add(formFieldModel);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, (int) (lines * Form.LINE_HEIGHT), 0, 0);
        formFieldModel.getRootView().setLayoutParams(layoutParams);
        fieldsContainer.addView(formFieldModel.getRootView());
        lines += formFieldModel.getLines();
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, (int) ((lines + 1) * Form.LINE_HEIGHT));
        layoutParams1.weight = 1;
        fieldsContainer.setLayoutParams(layoutParams1);
    }

    void notifyLineChanges(FormFieldView formFieldView) {
        int indexOfFormFieldView = fields.indexOf(formFieldView);
        int lines = 0;
        for (int i = 0; i < fields.size(); i++) {
            FormFieldView field = fields.get(i);
            int fieldLines = field.getLines();
            if (!field.isValid()) {
                fieldLines++;
            }
            if (i >= indexOfFormFieldView) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) ((fieldLines + 1) * Form.LINE_HEIGHT));
                layoutParams.setMargins(0, (int) (lines * Form.LINE_HEIGHT), 0, 0);
                fields.get(i).getRootView().setLayoutParams(layoutParams);
            }
            lines += fieldLines;
        }
        this.lines = lines;
        setVisible(this.lines != 0);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, (int) ((lines + 1) * Form.LINE_HEIGHT));
        layoutParams1.weight = 1;
        fieldsContainer.setLayoutParams(layoutParams1);
        form.notifyLineChanges(this);
    }

    void setForm(Form form) {
        this.form = form;
    }

    int getLines() {
        return lines;
    }

    public ArrayList<FormFieldView> getFields() {
        return fields;
    }

    public void animations(){
        for(int i=0;i<fields.size();i++){
            animate(fields.get(i).getRootView(), Animator.ANIMATION_FADE_IN);
        }
    }
    @Override
    public void onDestroy(){
        fieldsContainer  =null;
        for(FormFieldView fieldView:fields){
            fieldView.destroy();
        }
        fields.clear();
        fields = null;
    }
}
