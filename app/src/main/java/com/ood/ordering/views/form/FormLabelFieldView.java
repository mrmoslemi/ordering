package com.ood.ordering.views.form;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;


/**
 * Created by Lenovo on 5/7/2017.
 */
public class FormLabelFieldView extends FormFieldView{
    private int titleResID;
    private String value;
    public FormLabelFieldView(Context context, int titleResID, String value) {
        super(context);
        this.titleResID = titleResID;
        this.value = value;
        setView();
    }

    @Override
    public View generateFieldView(ViewGroup parent) {
        View toReturn = inflate(R.layout.form_select_field_view,parent);
        TextView valueTextView = (TextView) toReturn.findViewById(R.id.form_select_field_value);
        TextView titleTextView = (TextView) toReturn.findViewById(R.id.form_select_field_title);
        valueTextView.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        titleTextView.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        valueTextView.setText(value);
        titleTextView.setText(titleResID);
        return toReturn;
    }
    @Override
    public int getLines() {if (visible) {
        return 1;
    } else {
        return 0;
    }
    }
}
