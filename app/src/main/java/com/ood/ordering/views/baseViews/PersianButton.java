package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class PersianButton extends Button {
    public PersianButton(Context context) {
        super(context);
        if (!isInEditMode()) {
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(0));
        }
    }

    public PersianButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            int style = getTypeface().getStyle();
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(style));
        }
    }

    public PersianButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            int style = getTypeface().getStyle();
            setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(style));
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text != null)
            text = FormatHelper.toPersianNumber(text.toString());
        super.setText(text, type);

    }
}
