package com.ood.ordering.views.popups;

import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import com.ood.ordering.R;
import com.ood.ordering.controller.JalaliCalendar;
import com.ood.ordering.models.Day;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.FormatHelper;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/15/2017.
 * form date select popup view
 */
public class FormSelectDatePopupView extends SelectPopupView implements NumberPicker.OnValueChangeListener {
    private NumberPicker datePicker;
    private NumberPicker monthPicker;
    private NumberPicker yearPicker;
    private int startYear;
    private int endYear;

    public FormSelectDatePopupView(View rootView, ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        super(rootView, choices, defaultValue, listener);
        Day startDay = (Day) choices.get(0);
        Day endDate = (Day) choices.get(1);
        setDays();
        setMonths();
        startYear = startDay.getYear();
        endYear = endDate.getYear();
        setYears();
    }

    @Override
    public int getLayoutID() {
        return R.layout.select_date_popup_layout;
    }

    @Override
    public void initialViews() {
        datePicker = (NumberPicker) findViewByID(R.id.select_date_date_popup_picker);
        initialPicker(datePicker);
        monthPicker = (NumberPicker) findViewByID(R.id.select_date_month_popup_picker);
        initialPicker(monthPicker);
        yearPicker = (NumberPicker) findViewByID(R.id.select_date_year_popup_picker);
        initialPicker(yearPicker);
        datePicker.setOnValueChangedListener(this);
        monthPicker.setOnValueChangedListener(this);
        yearPicker.setOnValueChangedListener(this);
    }

    @Override
    protected void onDestroy() {
        datePicker = null;
        monthPicker = null;
        yearPicker = null;
    }

    private void initialPicker(NumberPicker picker) {
        picker.setTextSize(R.dimen.text_size_m);
        picker.setWrapSelectorWheel(false);
        picker.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.NORMAL));
    }


    private void setYears() {
        int count = endYear - startYear + 1;
        String[] values = new String[count];
        for (int i = 0; i < count; i++) {
            values[i] = FormatHelper.toPersianNumber(String.valueOf(i + startYear));
        }
        yearPicker.setMinValue(0);
        yearPicker.setMaxValue(count - 1);
        yearPicker.setValue(0);
        Log.e("popup",count+"");
        yearPicker.setDisplayedValues(values);


    }

    private void setDays() {
        String[] values = new String[31];
        for (int i = 0; i < 31; i++) {
            values[i] = FormatHelper.toPersianNumber(String.valueOf(i + 1));
        }
        datePicker.setDisplayedValues(values);
        datePicker.setMinValue(0);
        datePicker.setMaxValue(30);
        datePicker.setValue(0);
    }

    private void setMonths() {
        int count = 12;
        String[] values = new String[count];
        values[0] = "فروردین";
        values[1] = "اردیبهشت";
        values[2] = "خرداد";
        values[3] = "تیر";
        values[4] = "مرداد";
        values[5] = "شهریور";
        values[6] = "مهر";
        values[7] = "آبان";
        values[8] = "آذر";
        values[9] = "دی";
        values[10] = "بهمن";
        values[11] = "اسفند";

        monthPicker.setMinValue(0);
        monthPicker.setMaxValue(count - 1);
        monthPicker.setDisplayedValues(values);
        monthPicker.setValue(0);
    }

    @Override
    protected void onAccept() {
        int day = datePicker.getValue() + 1;
        int month = monthPicker.getValue() + 1;
        int year = yearPicker.getValue() + startYear;
        Day dateModel = new Day();
        dateModel.date = JalaliCalendar.jalaliToGregorian(year, month, day);
        Log.e("DATE", dateModel.date);
        onSelect(dateModel);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        if (picker == monthPicker) {
            int oldDays;
            if (oldVal >= 0 && oldVal <= 5) {
                oldDays = 31;
            } else if (oldVal == 11 && (yearPicker.getValue() + startYear) % 4 != 3) {
                oldDays = 29;
            } else {
                oldDays = 30;
            }
            int newDays;
            if (newVal >= 0 && newVal <= 5) {
                newDays = 31;
            } else if (newVal == 11 && (yearPicker.getValue() + startYear) % 4 != 3) {
                newDays = 29;
            } else {
                newDays = 30;
            }
            if (newDays != oldDays) {
                int oldDay = datePicker.getValue();
                oldDay = oldDay % newDays;
                datePicker.setValue(oldDay);
                datePicker.setMaxValue(newDays - 1);
            }
        } else if (picker == yearPicker) {
            int newYear = newVal + startYear;
            if (monthPicker.getValue() == 11) {
                if (newYear % 4 != 3) {
                    if (datePicker.getValue() == 29) {
                        datePicker.setValue(0);
                    }
                    datePicker.setMaxValue(28);
                } else {
                    datePicker.setMaxValue(29);
                }
            }
        }
    }
}
