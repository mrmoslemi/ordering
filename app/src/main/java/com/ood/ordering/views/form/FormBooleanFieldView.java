package com.ood.ordering.views.form;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;


/**
 * Created by Lenovo on 2/28/2017.
 * form boolean fieald
 */

public class FormBooleanFieldView extends FormFieldView implements CompoundButton.OnCheckedChangeListener {
    private int titleResID;
    private boolean defaultValue;

    public FormBooleanFieldView(Context context, int titleResID, boolean defaultValue) {
        super(context);
        this.titleResID = titleResID;
        this.defaultValue = defaultValue;
        setView();
    }

    @Override
    public View generateFieldView(ViewGroup parent) {
        View toReturn = inflate(R.layout.form_boolean_field_view,parent);
        Switch has = (Switch) toReturn.findViewById(R.id.form_boolean_field_switch);
        TextView titleTextView = (TextView) toReturn.findViewById(R.id.form_boolean_field_title);
        titleTextView.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        has.setChecked(defaultValue);
        has.setOnCheckedChangeListener(this);
        titleTextView.setText(titleResID);
        return toReturn;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setValue(isChecked);
    }
    @Override
    public int getLines() {if (visible) {
        return 1;
    } else {
        return 0;
    }
    }
}
