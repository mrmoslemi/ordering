package com.ood.ordering.views.myViews;

import android.content.Context;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.models.review.Review;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 * view for review in list
 */
public class ReviewListItemView extends MyView {
    private Review review;

    private HorizontalTitleAndValueView customerTitleAndValue;
    private HorizontalTitleAndValueView rateTitleAndValue;
    private TextView contextTextView;

    public ReviewListItemView(Context context, Review review) {
        super(context, R.layout.review_list_item_view_layout);
        this.review = review;
        customerTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.review_list_item_customer), R.string.writer, this.review.customer.name);
        rateTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.review_list_item_rate), R.string.rate, String.valueOf(this.review.rate));
        contextTextView = (TextView) findViewById(R.id.review_list_item_context);

        FontHelper.setBold(contextTextView);
        contextTextView.setText(this.review.textReview);
    }

    @Override
    public void onDestroy() {
        this.review = null;

        if (this.customerTitleAndValue != null) {
            this.customerTitleAndValue.destroy();
            this.customerTitleAndValue = null;
        }
        if (this.rateTitleAndValue != null) {
            this.rateTitleAndValue.destroy();
            this.rateTitleAndValue = null;
        }
        this.contextTextView = null;
    }
}
