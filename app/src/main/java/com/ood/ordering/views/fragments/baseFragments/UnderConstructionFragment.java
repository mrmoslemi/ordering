package com.ood.ordering.views.fragments.baseFragments;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.MyFragment;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/8/2017.
 */

public class UnderConstructionFragment extends MyFragment {
    @Override
    public int getLayoutID() {
        return R.layout.under_cunstruction_fragment_layout;
    }

    @Override
    public void initialViews() {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void postActions() {

    }

    @Override
    public void initialArguments() {

    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    @Override
    protected String getLogTag() {
        return null;
    }
}
