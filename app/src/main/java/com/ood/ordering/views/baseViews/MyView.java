package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.YoYo;
import com.ood.ordering.controller.MainActivity;
import com.ood.ordering.controller.Requester.Request;

/**
 * Created by Lenovo on 3/17/2017.
 * my view parent
 */

abstract public class MyView {
    private View rootView;
    private LayoutInflater inflater;
    private Context context;

    public MyView(View rootView) {
        this.rootView = rootView;
        this.context = rootView.getContext();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public MyView(ViewGroup parent, Context context, int layoutID) {
        this.context = context;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rootView = inflate(layoutID, parent);
    }

    public MyView(Context context, int layoutID) {
        this.context = context;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rootView = inflate(layoutID, null);
    }


    public void setOnClickListener(View.OnClickListener listener) {
        if (rootView != null) {
            rootView.setFocusable(true);
            rootView.setClickable(true);
            rootView.setOnClickListener(listener);
        }
    }

    public View getRootView() {
        return this.rootView;
    }

    public LayoutInflater getInflater() {
        return this.inflater;
    }

    public Context getContext() {
        return this.context;
    }

    public String getString(int id) {
        return this.getContext().getString(id);
    }

    public Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(getContext(), id);
    }

    public View findViewById(int id) {
        return rootView.findViewById(id);
    }

    public View inflate(int layoutID, ViewGroup parent) {
        View toReturn = inflater.inflate(layoutID, parent, false);
        ((MainActivity) getContext()).setupUI(toReturn);
        return toReturn;
    }

    public void setVisible(boolean visible) {

        if (visible) {
            rootView.setVisibility(View.VISIBLE);
        } else {
            rootView.setVisibility(View.GONE);
        }
    }

    public MainActivity getMainActivity() {
        return (MainActivity) context;
    }

    protected void onDestroy() {

    }

    public void destroy() {
        onDestroy();
        rootView = null;
        inflater = null;
        context = null;
    }

    public void request(Request request) {
        getMainActivity().request(request);
    }

    private int delayOfsset;

    public void doAnimations(int delayOfsset) {
        this.delayOfsset = delayOfsset;
        animations();
    }

    protected void animations() {

    }

    public void animate(final View view, final int animationType, int delay) {
        final YoYo.AnimatorCallback onStart = new YoYo.AnimatorCallback() {
            @Override
            public void call(android.animation.Animator animator) {
                view.setVisibility(View.VISIBLE);
            }
        };
        final YoYo.AnimatorCallback onEnd;
        switch (animationType) {
            case Animator.ANIMATION_FADE_IN:
            case Animator.ANIMATION_ZOOM_IN:
            case Animator.ANIMATION_SLIDE_UP:
            case Animator.ANIMATION_SLIDE_LEFT:
                view.setVisibility(View.INVISIBLE);
                onEnd = null;
                break;
            case Animator.ANIMATION_FADE_OUT:
            case Animator.ANIMATION_ZOOM_OUT:
                onEnd = new YoYo.AnimatorCallback() {
                    @Override
                    public void call(android.animation.Animator animator) {
                        view.setVisibility(View.GONE);
                    }
                };
                break;
            default:
                onEnd = null;
        }
        getRootView().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          if (getRootView() != null) {
                                              Animator.animate(view, animationType, onStart, onEnd, Animator.TIME_SHORT);
                                          }
                                      }
                                  }, delay + delayOfsset
        );

    }

    public void animate(View view, int animationType) {
        animate(view, animationType, 0);
    }

}
