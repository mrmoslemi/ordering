package com.ood.ordering.views.form;

import android.content.Context;

import com.ood.ordering.models.Model;
import com.ood.ordering.views.popups.SelectListPopupView;
import com.ood.ordering.views.popups.SelectPopupView;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/14/2017.
 */

public class FormSelectListFieldView extends FormSelectFieldView {

    public FormSelectListFieldView(Context context, boolean editable, int titleResID, ArrayList<Model> choices, Model defaultValue) {
        super(context, editable, titleResID, choices, defaultValue);
        setView();
    }

    @Override
    public SelectPopupView getPopupView(ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        return new SelectListPopupView(getRootView(),choices,defaultValue,listener);
    }
}
