package com.ood.ordering.views.fragments;

import com.ood.ordering.views.baseViews.MyFragment;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class ComponentDetailsFragment extends MyFragment{
    public static final String COMPONENT = "component";
    @Override
    public int getLayoutID() {
        return 0;
    }

    @Override
    public void initialViews() {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void postActions() {

    }

    @Override
    public void initialArguments() {

    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    @Override
    protected String getLogTag() {
        return null;
    }
}
