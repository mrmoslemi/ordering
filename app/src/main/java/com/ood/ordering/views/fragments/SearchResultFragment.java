package com.ood.ordering.views.fragments;

import com.ood.ordering.R;
import com.ood.ordering.models.Model;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.fragments.baseFragments.ListShowFragment;
import com.ood.ordering.views.myViews.ComponentListItemView;
import com.ood.ordering.views.myViews.ProductListItemView;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/8/2017.
 * shows result of search
 */

public class SearchResultFragment extends ListShowFragment {
    public static final String COMPONENTS = "components";
    public static final String PRODUCTS = "products";
    private ArrayList<Model> responses;

    @Override
    public ArrayList<Model> getModels() {
        return responses;
    }

    @Override
    public int getTitle() {
        return R.string.search_result;
    }

    @Override
    public void initialArguments() {
        ArrayList<Model> components = (ArrayList<Model>) getArguments().getSerializable(COMPONENTS);
        ArrayList<Model> products = (ArrayList<Model>)getArguments().getSerializable(PRODUCTS);
        responses = new ArrayList<>();
        if(products!=null) {
            responses.addAll(products);
        }
        if(components!=null){
            responses.addAll(components);
        }

    }

    @Override
    protected String getLogTag() {
        return "Search result fragment";
    }

    @Override
    public MyView getView(Model model) {
        if(model instanceof Product){
            return  new ProductListItemView(getContext(),(Product)model);
        }else if(model instanceof Component){
            return new ComponentListItemView(getContext(),(Component)model);
        }else {
            throw new IllegalArgumentException();
        }
    }
}
