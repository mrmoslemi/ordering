package com.ood.ordering.views.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.models.review.Review;
import com.ood.ordering.views.baseViews.MyFragment;
import com.ood.ordering.views.myViews.CompareListItemView;
import com.ood.ordering.views.myViews.ComponentListItemView;
import com.ood.ordering.views.myViews.LinearListView;
import com.ood.ordering.views.myViews.ProductListItemView;
import com.ood.ordering.views.myViews.ReviewListItemView;
import com.ood.ordering.views.myViews.RoundButtonView;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/8/2017.
 * details of a product
 */
public class ProductDetailsFragment extends MyFragment implements View.OnClickListener {
    public static final String PRODUCT = "product";
    private Product product;
    private LinearLayout content;
    private ProductListItemView summaryView;
    private RoundButtonView orderButton;
    private RoundButtonView compareButton;
    private RoundButtonView reviewButton;
    private LinearListView components;
    private LinearListView compares;
    private LinearListView reviews;

    @Override
    public int getLayoutID() {
        return R.layout.product_details_fragment;
    }

    @Override
    public void initialViews() {
        this.content = (LinearLayout)findViewById(R.id.product_details_content);
        this.summaryView = new ProductListItemView(findViewById(R.id.product_details_summary), this.product);
        this.summaryView.setOnClickListener(null);
        this.orderButton = new RoundButtonView(findViewById(R.id.product_details_order), getStringFromResources(R.string.tab_order));
        this.compareButton = new RoundButtonView(findViewById(R.id.product_details_compare), getStringFromResources(R.string.compare));
        this.reviewButton = new RoundButtonView(findViewById(R.id.product_details_review), getStringFromResources(R.string.review));
        components = new LinearListView(getContext(), R.string.components);
        for (Component component : this.product.components) {
            components.addView(new ComponentListItemView(getContext(), component));
        }
        content.addView(components.getRootView());
        if (!(this.product.compares == null || this.product.compares.isEmpty())) {
            compares = new LinearListView(getContext(),R.string.compares);
            for(Compare compare:this.product.compares){
                compares.addView(new CompareListItemView(getContext(),compare));
            }
            content.addView(compares.getRootView());
        }
        if (!(this.product.reviews == null || this.product.reviews.isEmpty())) {
            reviews = new LinearListView(getContext(),R.string.reviews);
            for(Review review:this.product.reviews){
                reviews.addView(new ReviewListItemView(getContext(),review));
            }
            content.addView(reviews.getRootView());
        }
    }

    @Override
    public void setListeners() {
        orderButton.setOnClickListener(this);
        compareButton.setOnClickListener(this);
        reviewButton.setOnClickListener(this);
    }

    @Override
    public void postActions() {
        getMainActivity().setTitle(this.product.name);
    }

    @Override
    public void initialArguments() {
        this.product = (Product) getArguments().getSerializable(PRODUCT);
    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    @Override
    protected String getLogTag() {
        return "product details fragment";
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if(content!=null){
            content.removeAllViews();
            content = null;
        }
        if(summaryView!=null){
            summaryView.destroy();
            summaryView = null;
        }
        if(orderButton!=null){
            orderButton.destroy();
            orderButton = null;
        }
        if(compareButton!=null){
            compareButton.destroy();
            compareButton = null;
        }
        if(reviewButton!=null){
            reviewButton.destroy();
            reviewButton = null;
        }
        if(components!=null){
            components.destroy();
            components = null;
        }
        if(compares!=null){
            compares.destroy();
            compares = null;
        }
        if(reviews!=null){
            reviews.destroy();
            reviews = null;
        }
    }

    @Override
    public void onClick(View v) {
        if(v==orderButton.getRootView()){
            OrderDoneFragment fragment = new OrderDoneFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(OrderDoneFragment.PRODUCT,this.product);
            fragment.setArguments(bundle);
            addFragment(fragment);
        }else if(v==reviewButton.getRootView()){
            ReviewFragment fragment = new ReviewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(ReviewFragment.PRODUCT,this.product);
            fragment.setArguments(bundle);
            addFragment(fragment);
        }else if(v==compareButton.getRootView()){
            CompareFragment fragment = new CompareFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CompareFragment.PRODUCT,this.product);
            fragment.setArguments(bundle);
            addFragment(fragment);
        }
    }
}
