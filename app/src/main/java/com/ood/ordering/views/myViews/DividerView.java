package com.ood.ordering.views.myViews;

import android.content.Context;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 * divider view
 */
public class DividerView extends MyView{
    private TextView titleTextView;
    public DividerView(Context context, int titleResID) {
        super(context, R.layout.divider_view_layout);
        titleTextView = (TextView)findViewById(R.id.divider_view_title);
        FontHelper.setBold(titleTextView);
        titleTextView.setText(titleResID);
    }

    @Override
    public void onDestroy(){
        titleTextView = null;
    }
}
