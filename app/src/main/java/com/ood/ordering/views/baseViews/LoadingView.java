package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ood.ordering.R;

/**
 * Created by Lenovo on 5/25/2017.
 */
public class LoadingView extends MyView implements View.OnClickListener {
    public static final int LOADING = 1;
    public static final int DONE = 2;
    public static final int SERVER_FAILURE = 3;
    private static final int INTERNET_FAILURE = 4;
    private View serverFailureView;
    private View loadingView;
    private View internetFailureView;
    private int state;
    private View.OnClickListener listener;

    public LoadingView(View rootView) {
        super(rootView);
        initial();
    }

    public LoadingView(Context context) {
        super(context, R.layout.regular_loading_view_layout);
        initial();
    }

    public LoadingView(Context context, int layoutResourceID) {
        super(context, layoutResourceID);
        initial();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getRootView().setLayoutParams(layoutParams);
    }

    private void initial() {
        serverFailureView = findViewById(R.id.loading_view_server_failure_container);
        loadingView = findViewById(R.id.loading_view_loading_container);
        internetFailureView = findViewById(R.id.loading_view_internet_failure_container);
        getRootView().setOnClickListener(this);
        setState(LOADING);
    }

    public void setState(int state) {
        if (state / 100 == 4 || state / 100 == 5) {
            state = SERVER_FAILURE;
        } else if (state == 0) {
            state = INTERNET_FAILURE;
        }
        this.state = state;
        loadingView.setVisibility(View.INVISIBLE);
        serverFailureView.setVisibility(View.INVISIBLE);
        internetFailureView.setVisibility(View.INVISIBLE);
        setVisible(true);
        switch (state) {
            case LOADING:
                loadingView.setVisibility(View.VISIBLE);
                break;
            case SERVER_FAILURE:
                serverFailureView.setVisibility(View.VISIBLE);
                break;
            case INTERNET_FAILURE:
                internetFailureView.setVisibility(View.VISIBLE);
            case DONE:
                setVisible(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (state == SERVER_FAILURE && listener != null) {
            listener.onClick(v);
        }
    }

    public int getState() {
        return this.state;
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onDestroy() {
        listener = null;
        serverFailureView = null;
        loadingView = null;
    }
}


