package com.ood.ordering.views.myViews;

import android.content.Context;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;

/**
 * Created by Lenovo on 6/8/2017.
 * view for a compare in list
 */
public class CompareListItemView extends MyView {
    private Compare compare;

    private HorizontalTitleAndValueView comparedToTitleAndValue;
    private HorizontalTitleAndValueView customerTitleAndValue;
    private TextView contextTextView;

    public CompareListItemView(Context context, Compare compare) {
        super(context, R.layout.compare_list_item_view_layout);
        this.compare = compare;
        comparedToTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.compare_list_item_compared_to), R.string.compared_to, this.compare.comparedTo.name);
        customerTitleAndValue = new HorizontalTitleAndValueView(findViewById(R.id.compare_list_item_customer), R.string.writer, this.compare.customer.name);
        contextTextView = (TextView) findViewById(R.id.compare_list_item_context);

        FontHelper.setBold(contextTextView);
        contextTextView.setText(this.compare.context);
    }

    @Override
    public void onDestroy() {
        this.compare = null;
        this.contextTextView = null;
        if (this.comparedToTitleAndValue != null) {
            this.comparedToTitleAndValue.destroy();
            this.comparedToTitleAndValue = null;
        }
        if (this.customerTitleAndValue != null) {
            this.customerTitleAndValue.destroy();
            this.customerTitleAndValue = null;
        }
    }
}
