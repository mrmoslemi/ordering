package com.ood.ordering.views.fragments.baseFragments;

import android.util.Pair;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;

import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Request;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.LoadingView;
import com.ood.ordering.views.baseViews.MyFragment;

import java.util.ArrayList;
import java.util.HashMap;



/**
 * Created by Lenovo on 4/9/2017.
 * query fragment
 */

public abstract class QueryFragment extends MyFragment implements Requester.ObjectWaiter, Requester.ListWaiter {
    private LoadingView loadingView;
    private FrameLayout contentContainer;
    private HashMap<String, Pair<Model, Integer>> objectData;
    private HashMap<String, Pair<ArrayList<Model>, Integer>> listData;

    private SparseArray<String> requestIDs;
    public boolean shouldFetch = true;
    private boolean isFetching;
    private int responseCount;
    private int requestCount;

    @Override
    public int getLayoutID() {
        return R.layout.new_query_fragment_layout;
    }

    @Override
    public void initialViews() {
        loadingView = new LoadingView(findViewById(R.id.new_query_fragment_loading));
        contentContainer = (FrameLayout) findViewById(R.id.new_query_fragment_content);
        if (shouldFetch) {
            fetch();
        } else if (!isFetching) {
            done();

        }
    }

    private void done() {
        isFetching = false;
        if (loadingView != null) {
            log("doneeeeeeeeeexxxxxxxxxxxxxx");
            loadingView.setState(LoadingView.DONE);
            contentContainer.setVisibility(View.VISIBLE);
            View contentView = inflate(getContentLayoutID());
            contentContainer.removeAllViews();
            contentContainer.addView(contentView);
            finishedLoading();
        }
    }

    public abstract int getContentLayoutID();

    protected abstract void finishedLoading();

    private void fetch() {
        shouldFetch = false;
        isFetching = true;
        loadingView.setState(LoadingView.LOADING);
        contentContainer.setVisibility(View.GONE);
        final HashMap<String, Request> requests = getRequests();
        if (requests != null && requests.size()!=0) {
            requestCount = requests.size();
            responseCount = 0;
            requestIDs = new SparseArray<>();
            listData = new HashMap<>();
            objectData = new HashMap<>();
            getRootView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (String key : requests.keySet()) {
                        int id = requests.get(key).getID();
                        requestIDs.put(id, key);
                        request(requests.get(key));
                    }
                }
            }, 200);
        } else {
            done();
        }

    }

    @Override
    public void setListeners() {

    }

    protected abstract HashMap<String, Request> getRequests();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        loadingView = null;
        contentContainer = null;
    }

    @Override
    public void onSuccess(int statusCode, int id, ArrayList<Model> response) {
        responseCount++;
        String tag = requestIDs.get(id);
        Pair<ArrayList<Model>, Integer> result = new Pair<>(response, statusCode);
        listData.put(tag, result);
        if (responseCount == requestCount) {
            done();
        }
    }

    @Override
    public void onSuccess(int statusCode, int id, Model response) {
        responseCount++;
        String tag = requestIDs.get(id);
        Pair<Model, Integer> result = new Pair<>(response, statusCode);
        objectData.put(tag, result);
        if (responseCount == requestCount) {
            done();
        }
    }

    @Override
    public void onFailure(int statusCode, int id) {
        if (statusCode % 100 == 2) {
            onSuccess(statusCode, id, (Model) null);
        } else {
            shouldFetch = true;
            if (loadingView != null) {
                loadingView.setState(statusCode);
                contentContainer.setVisibility(View.GONE);
                loadingView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fetch();
                    }
                });
            }
        }
    }

    protected Pair<ArrayList<Model>, Integer> getList(String tag) {
        return listData.get(tag);
    }

    protected Pair<Model, Integer> getObject(String tag) {
        return objectData.get(tag);
    }

}
