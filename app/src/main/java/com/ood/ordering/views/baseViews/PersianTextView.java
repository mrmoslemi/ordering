package com.ood.ordering.views.baseViews;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.ood.ordering.R;

/**
 * Created by Lenovo on 5/25/2017.
 */

public class PersianTextView extends TextView {
    public PersianTextView(Context context) {
        super(context);
        if (!isInEditMode()) {
            //  setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(Typeface.BOLD));
            setTextColor(getResources().getColor(R.color.black));
            setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_m));
        }

    }

    public PersianTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            //   setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(Typeface.BOLD));
            if (attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textColor") == null) {
                setTextColor(getContext().getColor(R.color.black));
            }
            if (attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textSize") == null) {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_m));
            }
        }
    }

    public PersianTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            //      setTypeface(FontHelper.getInstance(context).getPersianTextTypeface(Typeface.BOLD));
            if (attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textColor") == null) {
                setTextColor(getContext().getColor(R.color.black));
            }
            if (attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textSize") == null) {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_m));
            }
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
//        int style = getTypeface().getStyle();
//        setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(style));
        if (text != null)
            text = FormatHelper.toPersianNumber(text.toString());
        super.setText(text, type);

    }

}