package com.ood.ordering.views.form;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.MyView;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/26/2017.
 * form field parent class
 */

public abstract class FormFieldView extends MyView {
    private LinearLayout errorsContainer;
    private Form.FormHandler handler;
    private Serializable value;
    private FormFieldGroupView group;
    protected boolean visible = true;

    public void setVisible(boolean visible) {
        this.visible = visible;
        if (visible) {
            getRootView().setVisibility(View.VISIBLE);
        } else {
            getRootView().setVisibility(View.GONE);
        }
        if (getGroup() != null) {
            getGroup().notifyLineChanges(this);
        }
    }

    public FormFieldView(Context context) {
        super(context, R.layout.form_field_view_layout);
        this.errorsContainer = (LinearLayout) findViewById(R.id.form_field_view_errors_container);
    }

    void setView() {
        view = generateFieldView((RelativeLayout) getRootView());
        ((RelativeLayout) getRootView()).addView(view);
    }

    private View view;

    public abstract View generateFieldView(ViewGroup parent);

    public void clearErrors() {
        this.errorsContainer.removeAllViews();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);
        view.setLayoutParams(layoutParams);
        group.notifyLineChanges(this);
    }

    public void addError(String newError) {
        if (this.errorsContainer.getChildCount() == 0) {
            this.errorsContainer.addView(new FormErrorView(getContext(), newError,errorsContainer).getRootView());
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
            layoutParams.setMargins(0, (int) (Form.LINE_HEIGHT), 0, 0);
            view.setLayoutParams(layoutParams);
            group.notifyLineChanges(this);
        }
    }

    public void setValue(Serializable value) {
        this.value = value;
        clearErrors();
        handler.notifyChange(this, value);
    }

    public Serializable getValue() {
        return this.value;
    }

    boolean isValid() {
        return errorsContainer.getChildCount() == 0;
    }

    public void setHandler(Form.FormHandler handler) {
        this.handler = handler;
    }

    public abstract int getLines();

    void setGroup(FormFieldGroupView group) {
        this.group = group;
    }

    protected FormFieldGroupView getGroup() {
        return group;
    }
}
