package com.ood.ordering.views.myViews;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.popups.SelectPopupView;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class OverViewChoice extends MyView implements SelectPopupView.CheckableView {
    private CheckBox checkBox;

    public OverViewChoice(Context context, String titleText) {
        super(context, R.layout.over_view_choice_view_layout);
        this.checkBox = (CheckBox) findViewById(R.id.over_view_choice_checkbox);
        this.checkBox.setChecked(true);
        TextView title = (TextView) findViewById(R.id.over_view_choice_title);
        title.setTypeface(FontHelper.getInstance(getContext()).getPersianTextTypeface(Typeface.BOLD));
        title.setText(titleText);
    }

    @Override
    public void setChecked(boolean checked) {
        if (checked) {
            checkBox.setVisibility(View.VISIBLE);
        } else {
            checkBox.setVisibility(View.INVISIBLE);
        }
    }
}

