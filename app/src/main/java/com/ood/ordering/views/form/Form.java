package com.ood.ordering.views.form;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.RelativeLayout;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.Animator;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.myViews.RoundButtonView;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by Lenovo on 2/28/2017.
 * form class
 */


public class Form extends MyView {
    static float LINE_HEIGHT;

    public interface FormHandler {
        void notifyChange(FormFieldView field, Serializable newValue);
    }

    private int lines;

    public static final String NOT_SET = "وارد کردن فیلد زیر الزامی است.";
    public static final String PHONE_NUMBER_ERROR = "شماره تلفن همراه باید 11رقم باشد.";
    public static final String PASSWORD_SHORT = "طول گذرواژه باید حداقل 8 کاراکتر و شامل اعداد و حروف انگلیسی باشد.";
    public static final String PASSWORD_NOT_MATCH = "تکرار گذرواژه معتبر نیست.";
    public static final String WRONG_PASSWORD = "گذرواژه اشتباه است.";
    public static final String INVALID = "اطلاعات وارد شده صحیح نیست";
    public static final String INVALID_DATE = "روز پایان باید بعد از روز شروع باشد.";
    public static final String EMAIL_ERROR = "ایمیل اشتباه است.";


    private FormHandler handler;
    private RelativeLayout groupsContainer;
    private RoundButtonView deleteButton;
    private View buttonsContainer;
    private RoundButtonView refreshButton;
    private ArrayList<Pair<Integer, FormFieldGroupView>> groups;
    private RoundButtonView submitButton;
    private int extraLines = 0;

    public Form(Context context, FormHandler handler, int submitTitleResID) {
        super(context, R.layout.form_view_layout);
        if (LINE_HEIGHT == 0) {
            LINE_HEIGHT = getContext().getResources().getDimension(R.dimen.form_line_height);
        }
        this.lines = 0;
        this.groups = new ArrayList<>();
        this.handler = handler;
        this.groupsContainer = (RelativeLayout) findViewById(R.id.form_groups_container);
        this.buttonsContainer = findViewById(R.id.form_buttons_container);
        this.deleteButton = new RoundButtonView(findViewById(R.id.form_delete_button), getString(R.string.delete));
        this.refreshButton = new RoundButtonView(findViewById(R.id.form_refresh_button), getString(R.string.refresh));
        this.submitButton = new RoundButtonView(findViewById(R.id.form_accept_button), getString(submitTitleResID));
        extraLines++;
    }


    public void addField(FormFieldView field, int iconResID) {
        FormFieldGroupView groupView = null;
        int linesAbove = 0;
        for (int i = 0; i < groups.size(); i++) {
            Pair<Integer, FormFieldGroupView> pair = groups.get(i);
            if (pair.first == iconResID) {
                groupView = pair.second;
                break;
            }
            linesAbove += pair.second.getLines();
        }
        if (groupView == null) {
            groupView = new FormFieldGroupView(getContext(), iconResID);
            groupView.setForm(this);
            groups.add(new Pair<>(iconResID, groupView));
            groupsContainer.addView(groupView.getRootView());
        }
        groupView.addField(field);
        field.setHandler(handler);
        RelativeLayout.LayoutParams groupLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        groupLayoutParams.setMargins(0, (int) (linesAbove * LINE_HEIGHT), 0, 0);
        groupView.getRootView().setLayoutParams(groupLayoutParams);
        lines += field.getLines();
        RelativeLayout.LayoutParams groupsContainerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) ((lines + extraLines) * LINE_HEIGHT));
        groupsContainer.setLayoutParams(groupsContainerLayoutParams);

    }

    public boolean isValid() {
        for (Pair<Integer, FormFieldGroupView> pair : groups) {
            for (FormFieldView model : pair.second.getFields()) {
                if (!model.isValid()) {
                    return false;
                }
            }
        }
        return true;
    }

    void notifyLineChanges(FormFieldGroupView groupView) {
        int indexOfGroup = -1;
        for (int i = 0; i < groups.size(); i++) {
            Pair<Integer, FormFieldGroupView> pair = groups.get(i);
            if (pair.second == groupView) {
                indexOfGroup = i;
            }
        }
        int lines = 0;
        for (int i = 0; i < groups.size(); i++) {
            FormFieldGroupView group = groups.get(i).second;
            if (i >= indexOfGroup) {
                RelativeLayout.LayoutParams groupLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                groupLayoutParams.setMargins(0, (int) (LINE_HEIGHT * lines), 0, 0);
                group.getRootView().setLayoutParams(groupLayoutParams);
            }
            lines += group.getLines();
        }
        this.lines = lines;
        RelativeLayout.LayoutParams groupsContainerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) ((lines + extraLines) * LINE_HEIGHT));
        groupsContainer.setLayoutParams(groupsContainerLayoutParams);
    }

    public void setButtonsContainerVisible(){
        buttonsContainer.setVisibility(View.VISIBLE);
    }

    public RoundButtonView getDeleteButton() {
        return deleteButton;
    }

    public RoundButtonView getRefreshButton() {
        return refreshButton;
    }

    public RoundButtonView getSubmitButton() {
        return submitButton;
    }

    @Override
    public void animations() {
        for (int i = 0; i < groups.size(); i++) {
            groups.get(i).second.doAnimations(i * Animator.DELAY_SHORT);
        }
        if (extraLines == 1) {
            animate(submitButton.getRootView(), Animator.ANIMATION_SLIDE_LEFT, groups.size() * Animator.DELAY_SHORT);
        }
        if (buttonsContainer.getVisibility()== View.VISIBLE) {
            animate(buttonsContainer,Animator.ANIMATION_SLIDE_LEFT,(groups.size()+1)*Animator.DELAY_SHORT);
        }
    }
    @Override
    public void onDestroy(){
        handler = null;
        groupsContainer = null;
        deleteButton.destroy();
        deleteButton = null;
        buttonsContainer = null;
        refreshButton.destroy();
        refreshButton = null;
        for(Pair<Integer,FormFieldGroupView> group:groups){
            group.second.destroy();
        }
        groups.clear();
        groups = null;
        submitButton = null;
    }

}

