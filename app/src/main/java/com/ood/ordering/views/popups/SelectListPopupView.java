package com.ood.ordering.views.popups;

import android.view.View;
import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.models.Model;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.myViews.OverViewChoice;

import java.util.ArrayList;


/**
 * Created by Lenovo on 3/14/2017.
 * select list popup view
 */
public class SelectListPopupView extends SelectPopupView implements View.OnClickListener {
    private ArrayList<OverViewChoice> checkableViews;
    private LinearLayout linearLayout;

    public SelectListPopupView(View rootView, ArrayList<Model> choices, Model defaultValue, SelectPopupView.OnSelectListener listener) {
        super(rootView, choices, defaultValue, listener);
        linearLayout = (LinearLayout) findViewByID(R.id.multiple_choice_list);
        checkableViews = new ArrayList<>();
        for (Model model : getChoices()) {
            OverViewChoice overViewChoice = new OverViewChoice(getContext(), model.toString());
            if (model.equals(getDefaultValue())) {
                overViewChoice.setChecked(true);
            }
            overViewChoice.setOnClickListener(this);
            checkableViews.add(overViewChoice);
            linearLayout.addView(overViewChoice.getRootView());
        }
    }

    @Override
    public int getLayoutID() {
        return R.layout.select_list_popup_layout;
    }

    @Override
    protected void onAccept() {

    }

    @Override
    public void initialViews() {
    }

    @Override
    protected void onDestroy() {
        linearLayout = null;
        for (MyView view : checkableViews) {
            view.destroy();
        }
        checkableViews.clear();
        checkableViews = null;
    }

    @Override
    public void onClick(View v) {
        int index = -1;
        for (OverViewChoice checkableView : checkableViews) {
            if (checkableView.getRootView().equals(v)) {
                checkableView.setChecked(true);
                index = checkableViews.indexOf(checkableView);
            } else {
                checkableView.setChecked(false);
            }
        }
        onSelect(getChoices().get(index));
    }
}
