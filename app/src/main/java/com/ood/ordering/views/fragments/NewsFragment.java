package com.ood.ordering.views.fragments;

import com.ood.ordering.R;
import com.ood.ordering.controller.Requester.Requester;
import com.ood.ordering.models.Model;
import com.ood.ordering.models.object.Component;
import com.ood.ordering.models.object.Product;
import com.ood.ordering.models.person.Customer;
import com.ood.ordering.models.review.Compare;
import com.ood.ordering.models.review.Review;
import com.ood.ordering.views.baseViews.MyView;
import com.ood.ordering.views.fragments.baseFragments.ListFragment;
import com.ood.ordering.views.myViews.ProductListItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class NewsFragment extends ListFragment {
    @Override
    public Class<? extends Model> getObjectsClass() {
        return Product.class;
    }

    @Override
    public String getUrl() {
        return Requester.URL_NEW_PRODUCTS;
    }

    @Override
    public MyView getView(Model model) {
        return new ProductListItemView(getContext(),(Product)model);
    }

    @Override
    public void postActions() {
        getMainActivity().setTitle(R.string.tab_news);
    }

    @Override
    public void initialArguments() {

    }

    @Override
    public void itemSelected(int position) {

    }

    @Override
    public ArrayList<String> getOptions() {
        return null;
    }

    @Override
    protected String getLogTag() {
        return "new products fragment";
    }

    @Override
    public void onFailure(int statusCode, int id) {
        ArrayList<Model> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            List<Component> components = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                Component component = new Component((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, "مدل تست " + i);
                components.add(component);
            }
            List<Review> reviews = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                Customer customer = new Customer("محمد رضا مسلمی","09350746443","rmoslemi1994@gmail.com",(j+1)*100);
                Review review = new Review ("محصول بسیار خوب و کارایی هست و من به همه چیز امتیاز "+(j+1)+" میدهم.",j+1,customer);
                reviews.add(review);
            }

            List<Compare> compares = new ArrayList<>();
            for (int j = 0; j < i; j++) {
                Customer customer = new Customer("محمد رضا مسلمی","09350746443","rmoslemi1994@gmail.com",(j+1)*100);
                Compare compare = new Compare((Product)result.get(j),"متن تست برای مقایسه بین دو محصول فرضی در سامانه که با توجه به این که در نمایش متن های طولانی چشم نواز تر هستند این متن را طولانی می نویسیم.",customer);
                compares.add(compare);
            }
            Product product = new Product((i + 1) * 10000, (10 - i) * 10, "محصول تست " + i, components,reviews,compares, true);
            result.add(product);
        }
        onSuccess(statusCode, id, result);
    }
}
