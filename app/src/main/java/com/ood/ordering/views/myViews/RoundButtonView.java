package com.ood.ordering.views.myViews;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FontHelper;
import com.ood.ordering.views.baseViews.MyView;


/**
 * Created by Lenovo on 5/14/2017.
 */

public class RoundButtonView extends MyView {
    private TextView title;
    private String titleText;

    public RoundButtonView(Context context, int title) {
        super(context, R.layout.round_button_view_layout);
        this.title = (TextView) findViewById(R.id.round_button_text);
        this.titleText = getString(title);
        FontHelper.setBold(this.title);
        this.title.setText(title);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, getContext().getResources().getDimensionPixelSize(R.dimen.form_line_height_card));
        int margin = getContext().getResources().getDimensionPixelSize(R.dimen.padding_m);
        layoutParams.setMargins(margin, margin, margin, margin);
        layoutParams.weight = 1;
        getRootView().setLayoutParams(layoutParams);
    }
    public RoundButtonView(View rootView, String title) {
        super(rootView);
        this.titleText = title;
        this.title = (TextView) findViewById(R.id.round_button_text);
        FontHelper.setBold(this.title);
        this.title.setText(title);

    }

    @Override
    public void onDestroy() {
        title = null;
    }

    public void setLoading(boolean loading) {
        getRootView().setClickable(!loading);
        if(loading){
            title.setText(R.string.loading);
        }else {
            title.setText(titleText);
        }
    }

    public void setText(int text) {
        this.title.setText(text);
    }

    public void setEnabled(boolean enabled) {
        if(!enabled){
            getRootView().setAlpha(0.5f);
            getRootView().setEnabled(false);
        }else {
            getRootView().setAlpha(1);
            getRootView().setEnabled(true);
        }
    }
}
