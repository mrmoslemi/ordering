package com.ood.ordering.views.myViews;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.MyView;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/8/2017.
 */
public class LinearListView extends MyView{
    private ArrayList<MyView> myViews;
    private DividerView dividerView;

    public LinearListView(Context context,int titleResID) {
        super(context, R.layout.linear_list_view_layout);
        myViews = new ArrayList<>();
        dividerView = new DividerView(getContext(),titleResID);
        ((LinearLayout)getRootView()).addView(dividerView.getRootView());
        setVisible(false);
    }
    public void addView(MyView myView){
        setVisible(true);
        myViews.add(myView);
        ((LinearLayout)getRootView()).addView(myView.getRootView());
    }
    @Override
    public void onDestroy(){
        dividerView.destroy();
        dividerView = null;
        for(MyView myView:myViews){
            myView.destroy();
        }
        myViews.clear();
        myViews = null;
    }
}
