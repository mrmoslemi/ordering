package com.ood.ordering.views.form;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.ood.ordering.R;
import com.ood.ordering.views.baseViews.FormatHelper;


/**
 * Created by Lenovo on 2/26/2017.
 * form text field model
 */

public class FormTextFieldView extends FormFieldView implements TextWatcher {
    public static final int NUMBER = 1;
    public static final int PASSWORD = 2;

    private int placeHolder;
    private int lines;
    private String defaultValue;
    private int maxLength;
    private EditText formTextFieldText;
    private View card;
    private int inputType;
    private int lineCount;

    public FormTextFieldView(Context context, int lines, int maxLength, int placeHolderResID, String defaultValue) {
        super(context);
        this.placeHolder = placeHolderResID;
        this.lines = lines;
        this.defaultValue = defaultValue;
        this.maxLength = maxLength;
        this.inputType = -1;
        setView();
    }

    public FormTextFieldView(Context context, int lines, int maxLength, int placeHolderResID, String defaultValue, int inputType) {
        super(context);
        this.placeHolder = placeHolderResID;
        this.lines = lines;
        this.defaultValue = defaultValue;
        this.maxLength = maxLength;
        this.inputType = inputType;
        setView();
    }

    @Override
    public View generateFieldView(ViewGroup parent) {
        lineCount = 1;
        int layResID = R.layout.form_text_field_view_layout;
        if (inputType != -1) {
            if (inputType == NUMBER) {
                layResID = R.layout.form_number_field_view_layout;
            } else if (inputType == PASSWORD) {
                layResID = R.layout.form_password_field_view_layout;
            }
        }
        View toReturn = inflate(layResID, parent);
        formTextFieldText = (EditText) toReturn.findViewById(R.id.form_text_field_text);
        card = toReturn.findViewById(R.id.form_text_field_card);


        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        formTextFieldText.setFilters(fArray);

        if (defaultValue != null) {
            formTextFieldText.removeTextChangedListener(this);
            formTextFieldText.setText(defaultValue);

        }
        formTextFieldText.post(new Runnable() {
            @Override
            public void run() {
                int i = formTextFieldText.getLineCount();
                Log.e("DEFAILT LINES","DDD"+i);
                if(i>lineCount){
                    lineCount = i;
                }
                formTextFieldText.setHint(placeHolder);
                if(lines==1){
                    formTextFieldText.setMaxLines(1);
                }
                setLineCount();
            }
        });
        formTextFieldText.addTextChangedListener(this);

        return toReturn;
    }

    @Override
    public int getLines() {
        if (visible) {
            return lineCount;
        } else {
            return 0;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s == formTextFieldText.getEditableText()) {
            if (lines == 1) {
                for (int i = s.length() - 1; i >= 0; i--) {
                    if (s.charAt(i) == '\n') {
                        s.delete(i, i + 1);
                        return;
                    }
                }
            }
            if (inputType == NUMBER) {
                formTextFieldText.removeTextChangedListener(this);
                formTextFieldText.setText(FormatHelper.toPersianNumber(s.toString()));
                formTextFieldText.setSelection(s.length());
                formTextFieldText.addTextChangedListener(this);
            }
            int newLineCount = formTextFieldText.getLineCount();
            if (lineCount != newLineCount) {
                lineCount = newLineCount;
                setLineCount();
                getGroup().notifyLineChanges(this);

            }
            setValue(FormatHelper.toEnglishNumber(s.toString(), inputType));
        }
    }

    public void setEditable(boolean editable) {
        formTextFieldText.setEnabled(editable);
    }


    private void setLineCount() {
        Log.e("LINES", "" + lineCount);
        if(lineCount>lines){
            lineCount = lines;
        }
        if(lineCount>4){
            lineCount = 4;
        }
        float normalPadding = getContext().getResources().getDimension(R.dimen.form_line_height_corner_margin);
        float cornerRadius = getContext().getResources().getDimension(R.dimen.form_line_height_corner_outer);
        formTextFieldText.setLines(lineCount);
        RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) ((lineCount + 1) * Form.LINE_HEIGHT));
        formTextFieldText.setLayoutParams(textLayoutParams);
        formTextFieldText.setPadding((int) (normalPadding + cornerRadius), (int) normalPadding, (int) (normalPadding + cornerRadius), (int) (normalPadding + Form.LINE_HEIGHT));
        RelativeLayout.LayoutParams cardLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) ((lineCount) * Form.LINE_HEIGHT));
        card.setLayoutParams(cardLayoutParams);
        card.setPadding((int) normalPadding, (int) normalPadding, (int) normalPadding, (int) normalPadding);

    }

    @Override
    public void addError(String error) {
        super.addError(error);
        setLineCount();
    }

    @Override
    public void clearErrors() {
        super.clearErrors();
        setLineCount();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        setLineCount();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        formTextFieldText.removeTextChangedListener(this);
        formTextFieldText = null;
    }

}
